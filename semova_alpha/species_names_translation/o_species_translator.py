#########################################################
#######  list of rxncon species labels (raw) #######
#########################################################
'''
0 Fus3-Ste5(MAPK)
1 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11
2 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20
3 Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)+Gpa1(GnP)-P
4 Ste2(CyT)-Sst2(n)(DEP)+MFalpha-Ste2(Receptor)
5 Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20
6 Ste2(CyT)-Sst2(n)(DEP)
7 Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)
8 Gpa1(GnP)-P
9 Gpa1(BD:Rec)-Ste2(CyT)+Gpa1(GnP)-P+Ste2(CyT)-Sst2(n)(DEP)
10 Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11
11 Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11
12 Fus3(CD)-Ste7(BD:MAPK)+Fus3(T180)-P+Fus3(Y182)-P+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11
13 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11
14 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste7-Ste11
15 Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11
16 Fus3(T180)-P+Fus3(Y182)-P
17 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18
18 Gpa1-Ste4(BD:Gpa1)
19 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(T363)-P+Ste7-Ste11
20 Dig2(Dsite)-Fus3(CD)(7m)
21 Dig2-Ste12(n)(DBD)
22 Dig2-P
23 Dig1-Ste12(c)+Dig2-Ste12(n)(DBD)
24 Ste4(BD:Ste5)-Ste5(n)(RING-H2)
25 Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)+MFalpha-Ste2(Receptor)
26 Ste5(MEKK)-Ste11
27 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11
28 MFalpha-Ste2(Receptor)
29 Ste2(TMD)-Ste2(TMD)
30 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20
31 Gpa1-Ste4(BD:Gpa1)+Ste4-Ste18
32 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11
33 Fus3(CD)-Ste7(BD:MAPK)+Fus3(T180)-P+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11
34 Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11
35 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste5(BD:Ste5)-Ste5(BD:Ste5)
36 Fus3(CD)-Ste7(BD:MAPK)+Ste7-Ste11
37 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11
38 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)
39 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11
40 Gpa1(BD:Rec)-Ste2(CyT)
41 Gpa1(BD:Rec)-Ste2(CyT)+Gpa1(GnP)-P
42 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11
43 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11
44 Ste4-Ste18
45 Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)+Ste2(CyT)-Sst2(n)(DEP)+Gpa1(GnP)-P
46 Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5(MEKK)-Ste11+Ste7-Ste11
47 Dig1-Ste12(c)
48 Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)+Gpa1(GnP)-P
49 Fus3(CD)-Ste7(BD:MAPK)
50 Dig1-P
51 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S306)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11
52 Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11
53 Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)
'''
################################################################
#######  list of translated species: ID, raw, translated #######
################################################################
'''
0 Fus3-Ste5(MAPK) Fus3-Ste5
1 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11 ComplexC+Ste11PPP+Ste7PPP
2 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S306)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11 ComplexC(Unlock)+Ste11(S306)P
3 Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)+Gpa1(GnP)-P Gpa1P-MFalpha-Ste2
4 Ste2(CyT)-Sst2(n)(DEP)+MFalpha-Ste2(Receptor) MFalpha-Sst2-Ste2
5 Fus3(CD)-Ste7(BD:MAPK)+Ste7-Ste11 Fus3-Ste7-Ste11
6 Ste2(CyT)-Sst2(n)(DEP) Sst2-Ste2
7 Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP) Gpa1-Sst2-Ste2
8 Gpa1(GnP)-P Gpa1P
9 Gpa1(BD:Rec)-Ste2(CyT)+Gpa1(GnP)-P+Ste2(CyT)-Sst2(n)(DEP) Gpa1P-Sst2-Ste2
10 Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11 Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11
11 Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5(MEKK)-Ste11+Ste7-Ste11 Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5(MEKK)-Ste11+Ste7-Ste11
12 Fus3(CD)-Ste7(BD:MAPK)+Fus3(T180)-P+Fus3(Y182)-P+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11 ComplexC(Unlock)+Ste11PPP+Ste7PP+Fus3PP
13 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11 ComplexC(Unlock)
14 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste7-Ste11 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste7-Ste11
15 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20 ComplexA+Ste5-Ste20
16 Fus3(T180)-P+Fus3(Y182)-P Fus3PP
17 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18 Ste4-Ste5-Ste18
18 Gpa1-Ste4(BD:Gpa1) Gpa1-Ste4
19 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11 ComplexC
20 Dig2(Dsite)-Fus3(CD)(7m) Dig2-Fus3
21 Dig2-Ste12(n)(DBD) Dig2-Ste12
22 Dig2-P Dig2P
23 Dig1-Ste12(c)+Dig2-Ste12(n)(DBD) Dig1-Dig2-Ste12
24 Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)+MFalpha-Ste2(Receptor) Gpa1-MFalpha-Sst2-Ste2
25 Ste5(MEKK)-Ste11 Ste5-Ste11
26 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11 ComplexC(Unlock)+Ste11PPP+Ste7PP
27 MFalpha-Ste2(Receptor) MFalpha-Ste2
28 Ste2(TMD)-Ste2(TMD) 2Ste2
29 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20 ComplexA+Ste4-Ste20+Ste5-Ste20
30 Gpa1-Ste4(BD:Gpa1)+Ste4-Ste18 Gpa1-Ste4-Ste18
31 Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11 Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11
32 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11
33 Fus3(CD)-Ste7(BD:MAPK)+Fus3(T180)-P+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11 ComplexC(Unlock)+Ste11PPP+Ste7PP+Fus3(T180)P
34 Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11 Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11
35 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste5(BD:Ste5)-Ste5(BD:Ste5) ComplexA
36 Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20 ComplexB
37 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11
38 Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5) ComplexA+Ste4-Ste20
39 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(T363)-P+Ste7-Ste11 ComplexC(Unlock)+Ste11PPP+Ste7(T363)P
40 Gpa1(BD:Rec)-Ste2(CyT) Gpa1-Ste2
41 Gpa1(BD:Rec)-Ste2(CyT)+Gpa1(GnP)-P Gpa1P-Ste2
42 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11 ComplexC(Unlock)+Ste11(S302)P+Ste11(S306)-P
43 Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11 ComplexC(Unlock)+Ste11PPP
44 Ste4-Ste18 Ste4-Ste18
45 Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)+Ste2(CyT)-Sst2(n)(DEP)+Gpa1(GnP)-P Gpa1P-MFalpha-Sst2-Ste2
46 Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11 Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11
47 Dig1-P Dig1P
48 Dig1-Ste12(c) Dig1-Ste12
49 Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)+Gpa1(GnP)-P Gpa1P-Sst2-Ste2
50 Ste4(BD:Ste5)-Ste5(n)(RING-H2) Ste4-Ste5
51 Fus3(CD)-Ste7(BD:MAPK) Fus3-Ste7
52 Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11 Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11
53 Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor) Gpa1-MFalpha-Ste2
'''
##############################################
#######  species dictionary for import #######
##############################################
rxncon_species_dict = {'Fus3-Ste5(MAPK)': 'Fus3-Ste5', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11': 'ComplexC+Ste11PPP+Ste7PPP', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S306)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11': 'ComplexC(Unlock)+Ste11(S306)P', 'Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)+Gpa1(GnP)-P': 'Gpa1P-MFalpha-Ste2', 'Ste2(CyT)-Sst2(n)(DEP)+MFalpha-Ste2(Receptor)': 'MFalpha-Sst2-Ste2', 'Fus3(CD)-Ste7(BD:MAPK)+Ste7-Ste11': 'Fus3-Ste7-Ste11', 'Ste2(CyT)-Sst2(n)(DEP)': 'Sst2-Ste2', 'Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)': 'Gpa1-Sst2-Ste2', 'Gpa1(GnP)-P': 'Gpa1P', 'Gpa1(BD:Rec)-Ste2(CyT)+Gpa1(GnP)-P+Ste2(CyT)-Sst2(n)(DEP)': 'Gpa1P-Sst2-Ste2', 'Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11': 'Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11', 'Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5(MEKK)-Ste11+Ste7-Ste11': 'Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5(MEKK)-Ste11+Ste7-Ste11', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3(T180)-P+Fus3(Y182)-P+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11': 'ComplexC(Unlock)+Ste11PPP+Ste7PP+Fus3PP', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11': 'ComplexC(Unlock)', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste7-Ste11': 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste7-Ste11', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20': 'ComplexA+Ste5-Ste20', 'Fus3(T180)-P+Fus3(Y182)-P': 'Fus3PP', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18': 'Ste4-Ste5-Ste18', 'Gpa1-Ste4(BD:Gpa1)': 'Gpa1-Ste4', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11': 'ComplexC', 'Dig2(Dsite)-Fus3(CD)(7m)': 'Dig2-Fus3', 'Dig2-Ste12(n)(DBD)': 'Dig2-Ste12', 'Dig2-P': 'Dig2P', 'Dig1-Ste12(c)+Dig2-Ste12(n)(DBD)': 'Dig1-Dig2-Ste12', 'Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)+MFalpha-Ste2(Receptor)': 'Gpa1-MFalpha-Sst2-Ste2', 'Ste5(MEKK)-Ste11': 'Ste5-Ste11', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11': 'ComplexC(Unlock)+Ste11PPP+Ste7PP', 'MFalpha-Ste2(Receptor)': 'MFalpha-Ste2', 'Ste2(TMD)-Ste2(TMD)': '2Ste2', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20': 'ComplexA+Ste4-Ste20+Ste5-Ste20', 'Gpa1-Ste4(BD:Gpa1)+Ste4-Ste18': 'Gpa1-Ste4-Ste18', 'Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11': 'Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11': 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3(T180)-P+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11': 'ComplexC(Unlock)+Ste11PPP+Ste7PP+Fus3(T180)P', 'Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11': 'Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste5(BD:Ste5)-Ste5(BD:Ste5)': 'ComplexA', 'Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20': 'ComplexB', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11': 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)': 'ComplexA+Ste4-Ste20', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(T363)-P+Ste7-Ste11': 'ComplexC(Unlock)+Ste11PPP+Ste7(T363)P', 'Gpa1(BD:Rec)-Ste2(CyT)': 'Gpa1-Ste2', 'Gpa1(BD:Rec)-Ste2(CyT)+Gpa1(GnP)-P': 'Gpa1P-Ste2', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11': 'ComplexC(Unlock)+Ste11(S302)P+Ste11(S306)-P', 'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11': 'ComplexC(Unlock)+Ste11PPP', 'Ste4-Ste18': 'Ste4-Ste18', 'Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)+Ste2(CyT)-Sst2(n)(DEP)+Gpa1(GnP)-P': 'Gpa1P-MFalpha-Sst2-Ste2', 'Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11': 'Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', 'Dig1-P': 'Dig1P', 'Dig1-Ste12(c)': 'Dig1-Ste12', 'Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)+Gpa1(GnP)-P': 'Gpa1P-Sst2-Ste2', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)': 'Ste4-Ste5', 'Fus3(CD)-Ste7(BD:MAPK)': 'Fus3-Ste7', 'Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11': 'Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11', 'Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)': 'Gpa1-MFalpha-Ste2'}
