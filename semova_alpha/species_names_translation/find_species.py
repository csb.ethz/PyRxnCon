#!/usr/bin/env python

"""
find_species 
(auxiliary script)

Script to identify automatically generated rxncon labels of specific species nodes.

-----------------------------------------------------------------------------------
INPUT:

- List of elemental states which should be present in the wanted species:
	- 'searched_states'
	-> manually created

- Species node labels automatically generated in 'rxncon2graph.py':
	- 'species_nodes_bipart' from 'o_rxncon2graph.o_species'
	-> Copy 'o_species.py' into the 'species_translation' folder!	

----------------------------------------------------------------------------------
OUTPUT:

Terminal prints the node label of the wanted species. 

----------------------------------------------------------------------------------
Author: JL
Date: 14/11/3
"""

from o_species import species_nodes_bipart

# dic of elemental states (here for a convinient search of elemental states)
dic_of_species_states = {'Bar1': set(['Bar1']), 'MFalpha': set(['MFalpha', 'MFalpha-Ste2(Receptor)']), 'Fus3': set(['Fus3-Ste5(MAPK)', 'Fus3(Y182)-P', 'Fus3', 'Fus3(T180)-P', 'Fus3-Ste12', 'Fus3-Ste5(Unlock)', 'Far1(n)(BD:Fus3)-Fus3(CD)', 'Fus3(CD)-Ste7(BD:MAPK)', 'Dig1(Dsite)-Fus3(CD)(7m)', 'Fus3(n)-Gpa1(n)']), 'Sst2': set(['Sst2', 'Ste2(CyT)-Sst2(n)(DEP)', 'Sst2(S539)-P']), 'Ste20': set(['Ste20', 'Ste5-Ste20', 'Ste20(KD)-Ste20(CRIB)', 'Ste20(SerThr)-P', 'Ste4-Ste20(c)']), 'Ste5': set(['Fus3-Ste5(MAPK)', 'Ste5-P', 'Ste5', 'Ste5-Ste20', 'Fus3-Ste5(Unlock)', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)', 'Ste5(BD:Ste5)-Ste5(BD:Ste5)', 'Ste5(MEKK)-Ste11', 'Ste5(MEK)-Ste7']), 'Dig1': set(['Dig1-P', 'Dig1-Ste12(c)', 'Dig1(Dsite)-Fus3(CD)(7m)', 'Dig1']), 'Ste2': set(['Ste2', 'Ste2(TMD)-Ste2(TMD)', 'Gpa1(BD:Rec)-Ste2(CyT)', 'Ste2(CyT)-Sst2(n)(DEP)', 'MFalpha-Ste2(Receptor)']), 'Ste11': set(['Ste7-Ste11', 'Ste11(CBD)(T307)-P', 'Ste11(CBD)(S302)-P', 'Ste11', 'Ste5(MEKK)-Ste11', 'Ste11(CBD)-Ste11(KD)', 'Ste11(CBD)(S306)-P']), 'Ste12': set(['Ste12', 'Fus3-Ste12', 'Dig1-Ste12(c)', 'Ste12(n)(HTH)-PRE', 'Ste12-P']), 'Far1': set(['Far1(n)(BD:Fus3)-Fus3(CD)', 'Far1', 'Far1(T306)-P', 'Far1(n)(RING-H2)-Ste4']), 'Ste7': set(['Ste7-Ste11', 'Ste7(AL)(T363)-P', 'Ste5(MEK)-Ste7', 'Ste7(AL)(S359)-P', 'Ste7-P', 'Ste7', 'Fus3(CD)-Ste7(BD:MAPK)']), 'Ste4': set(['Gpa1-Ste4(BD:Gpa1)', 'Ste4-Ste18', 'Ste4-Ste20(c)', 'Ste4', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)', 'Far1(n)(RING-H2)-Ste4']), 'PRE': set(['PRE', 'Ste12(n)(HTH)-PRE']), 'Ste18': set(['Ste18', 'Ste4-Ste18']), 'Gpa1': set(['Gpa1-Ste4(BD:Gpa1)', 'Gpa1(BD:Rec)-Ste2(CyT)', 'Gpa1(GnP)-P', 'Fus3(n)-Gpa1(n)', 'Gpa1'])}


searched_states = ('Ste4(BD:Ste5)-Ste5(n)(RING-H2)', 'Ste4-Ste18', 'Ste4(BD:Ste5)-Ste5(n)(RING-H2)', 'Ste4-Ste18', 'Ste5(BD:Ste5)-Ste5(BD:Ste5)', \
'Ste4-Ste20(c)', 'Ste5-Ste20', 'Ste20(SerThr)-P', 'Fus3(CD)-Ste7(BD:MAPK)', 'Ste7-Ste11', 'Fus3-Ste5(MAPK)', 'Ste5(MEK)-Ste7', \
'Ste5(MEKK)-Ste11', 'Fus3-Ste5(Unlock)', 'Ste11(CBD)(S306)-P', 'Ste11(CBD)(S302)-P', 'Ste11(CBD)(T307)-P', 'Ste7(AL)(T363)-P', \
'Ste7(AL)(S359)-P','Fus3(T180)-P')
#'Ste4(BD:Ste5)-Ste5(n)(RING-H2)', 'Ste5(BD:Ste5)-Ste5(BD:Ste5)']
#searched_states = ['Ste11(CBD)(T307)-P', 'Ste11(CBD)(S302)-P']
#'Fus3-Ste5(Unlock)'
#Fus3PP = ['Fus3(Y182)-P', 'Fus3(T180)']

for species in species_nodes_bipart:

	species_states = []

	to_split = str(species)

	elemental_states = to_split.split('+')
	#print elemental_states
	for el in elemental_states:
		species_states.append(el)

	if set(species_states) == set(searched_states) and len(searched_states) == len(species_states):
		print species



# Ste11PP: Ste11(CBD)(S302)-P+Ste11(CBD)(T307)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste5(MEKK)-Ste11
#Ste11: Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste5(MEKK)-Ste11
#Ste7: Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEK)-Ste7
