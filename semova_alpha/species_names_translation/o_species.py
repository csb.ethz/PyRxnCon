# species = 6
pure_species_rxncon = set(['MFalpha', 'Ste2'])
species_nodes_bipart = set(['MFalpha', 'MFalpha-Ste2', '2Ste2', 'Ste2', 'MFalpha-Ste2(Receptor)+Ste2(TMD)-Ste2(TMD)', 'MFalpha-Ste2(Receptor)+MFalpha-Ste2(Receptor)+Ste2(TMD)-Ste2(TMD)'])
binding_states = set(['Ste2(TMD)-Ste2(TMD)', 'MFalpha-Ste2(Receptor)'])
all_elemental_states= set(['Ste2(TMD)-Ste2(TMD)', 'MFalpha-Ste2(Receptor)'])
