#!/usr/bin/env python

"""
species_translator
(auxiliary script)

Script to create a dictionary of species names
- for the translation of the automatically generated rxncon species names 
  into model species names (they have to be mapped manually)

 -----------------------------------------------------------------------------------------
INPUT:
List of untranslated rxncon species (automatically generated) and
a list of translated rxncon species with the same order (manually created):
	- rxncon_species_raw
	- rxncon_species_translated

-----------------------------------------------------------------------------------------
OUTPUT: 
Dictionary 'rxncon_species_dict': 
- untranslated species names are keys, translated species names are the respective values

-----------------------------------------------------------------------------------------
NECESSARY FOR:
- species nodes mapping (cf. Chapter 3 in thesis)

-----------------------------------------------------------------------------------------
TODO:
- Supplement with names of species in McClean and Schaber models

-----------------------------------------------------------------------------------------
Author: JL
Date: 14/6/-
"""

output = open('o_species_translator.py', 'w+')

# set of rxncon species node labels (automatically generated)
rxncon_species_nodes = set([\
	'Gpa1-Ste4(BD:Gpa1)+Ste4-Ste18',\
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3(T180)-P+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3(T180)-P+Fus3(Y182)-P+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11', \
	'Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(T363)-P+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S306)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7-Ste11', \
	'Fus3(T180)-P+Fus3(Y182)-P', \
	'Dig2(Dsite)-Fus3(CD)(7m)',\
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5(MEKK)-Ste11+Ste7-Ste11', \
	'Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Ste7-Ste11', \
	'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11', \
	'Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11', \
	'Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11',\
	'Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', \
	'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20', \
	'Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20',\
	'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5-Ste20',\
	'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)', \
	'Dig2-Ste12(n)(DBD)', \
	'Dig1-Ste12(c)+Dig2-Ste12(n)(DBD)', \
	'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18',\
	'Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste5(BD:Ste5)-Ste5(BD:Ste5)',\
	'Fus3(CD)-Ste7(BD:MAPK)',\
	'Dig1-P',\
	'Dig2-P', \
	'Ste5(MEKK)-Ste11',\
	'Ste4-Ste18',\
	'Gpa1-Ste4(BD:Gpa1)',\
	'Ste4(BD:Ste5)-Ste5(n)(RING-H2)',\
	'Dig1-Ste12(c)', \
	'Ste5(MEKK)-Ste11', \
	'Fus3-Ste5(MAPK)', \
	'Gpa1(BD:Rec)-Ste2(CyT)+Gpa1(GnP)-P+Ste2(CyT)-Sst2(n)(DEP)', \
	'Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)+Gpa1(GnP)-P', \
	'Ste2(CyT)-Sst2(n)(DEP)+MFalpha-Ste2(Receptor)', \
	'Gpa1(BD:Rec)-Ste2(CyT)+Gpa1(GnP)-P', \
	'Ste2(CyT)-Sst2(n)(DEP)', \
	'Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)', \
	'Gpa1(GnP)-P', \
	'Ste2(TMD)-Ste2(TMD)', \
	 'Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)+MFalpha-Ste2(Receptor)', \
	 'Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)+Ste2(CyT)-Sst2(n)(DEP)+Gpa1(GnP)-P', \
	 'MFalpha-Ste2(Receptor)', \
	 'Gpa1(BD:Rec)-Ste2(CyT)', \
	 'Gpa1(BD:Rec)-Ste2(CyT)+MFalpha-Ste2(Receptor)', \
	 'Gpa1(BD:Rec)-Ste2(CyT)+Ste2(CyT)-Sst2(n)(DEP)+Gpa1(GnP)-P'])


# create a list for zipping
rxncon_species_raw = []
for species in rxncon_species_nodes:
    rxncon_species_raw.append(species)

# print enumerated list of species (for species order identification)
print >> output, '#########################################################'
print >> output, '#######  list of rxncon species labels (raw) #######'
print >> output, '#########################################################'
print >> output, "'''"
n = 0
for species in rxncon_species_nodes:
	print >> output, n, species
	n += 1
print >> output, "'''"

# list of translated species names (manually created)
# (species should have the same order as in printed list above)

rxncon_species_translated = [\
'Fus3-Ste5',\
'ComplexC+Ste11PPP+Ste7PPP',\
'ComplexA+Ste5-Ste20', \
'Gpa1P-MFalpha-Ste2',\
'MFalpha-Sst2-Ste2',\
'ComplexB',\
'Sst2-Ste2',\
'Gpa1-Sst2-Ste2',\
'Gpa1P',\
'Gpa1P-Sst2-Ste2',\
'Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11', \
'Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', \
'ComplexC(Unlock)+Ste11PPP+Ste7PP+Fus3PP',\
'ComplexC(Unlock)',\
'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste7-Ste11', \
'Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11', \
'Fus3PP', \
'Ste4-Ste5-Ste18', \
'Gpa1-Ste4', \
'ComplexC(Unlock)+Ste11PPP+Ste7(T363)P',\
'Dig2-Fus3', \
'Dig2-Ste12', \
'Dig2P', \
'Dig1-Dig2-Ste12',\
'Ste4-Ste5', \
'Gpa1-MFalpha-Sst2-Ste2', \
'Ste5-Ste11', \
'ComplexC(Unlock)+Ste11PPP+Ste7PP',\
'MFalpha-Ste2', \
'2Ste2', \
'ComplexA+Ste4-Ste20+Ste5-Ste20',\
'Gpa1-Ste4-Ste18',\
'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste7-Ste11',\
'ComplexC(Unlock)+Ste11PPP+Ste7PP+Fus3(T180)P',\
'Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11',\
'ComplexA',\
'Fus3-Ste7-Ste11',\
'Fus3(CD)-Ste7(BD:MAPK)+Fus3-Ste5(MAPK)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11',\
'ComplexA+Ste4-Ste20',\
'ComplexC',\
'Gpa1-Ste2', \
'Gpa1P-Ste2', \
'ComplexC(Unlock)+Ste11(S302)P+Ste11(S306)-P', \
'ComplexC(Unlock)+Ste11PPP', \
'Ste4-Ste18', \
'Gpa1P-MFalpha-Sst2-Ste2', \
'Fus3(CD)-Ste7(BD:MAPK)+Ste5(MEK)-Ste7+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5(MEKK)-Ste11+Ste7-Ste11',\
'Dig1-Ste12',\
'Gpa1P-Sst2-Ste2', \
'Fus3-Ste7', \
'Dig1P', \
'ComplexC(Unlock)+Ste11(S306)P',\
'Fus3-Ste5(MAPK)+Fus3-Ste5(Unlock)+Ste11(CBD)(S302)-P+Ste11(CBD)(S306)-P+Ste11(CBD)(T307)-P+Ste20(SerThr)-P+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4(BD:Ste5)-Ste5(n)(RING-H2)+Ste4-Ste18+Ste4-Ste18+Ste4-Ste20(c)+Ste5(BD:Ste5)-Ste5(BD:Ste5)+Ste5(MEK)-Ste7+Ste5(MEKK)-Ste11+Ste5-Ste20+Ste7(AL)(S359)-P+Ste7(AL)(T363)-P+Ste7-Ste11',\
'Gpa1-MFalpha-Ste2', \
]

rxncon_species_dict = {}
rxncon_species_dict = dict(zip(rxncon_species_raw, rxncon_species_translated))


print >> output, '################################################################'
print >> output, '#######  list of translated species: ID, raw, translated #######'
print >> output, '################################################################'
print >> output, "'''"
n = 0
for key in rxncon_species_dict.keys():
	print >> output, n, key, rxncon_species_dict[key]
	n += 1
print >> output, "'''"

print >> output, '##############################################'
print >> output, '#######  species dictionary for import #######'
print >> output, '##############################################'
print >> output, 'rxncon_species_dict =', rxncon_species_dict

print 'rxncon_species_dict dictionary created!'