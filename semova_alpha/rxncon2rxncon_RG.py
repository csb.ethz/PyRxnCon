#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    rxncon2rxncon_RG - translate RxnCon data into the RxnCon regulatory graph


Usage:
    python rxncon2rxncon_RG.py rxncon_data_dict_mod


Attributes:
    rxncon_data_dict_mod:
        Path to a Python module containing `rxncon_data_dict`. Output of the
        `import_rxncon.py` script.


Options:
    -h, --help:
        Print this info.

    -o PATH, --output-dir=PATH:
        Output directory path, by default " o_rxncon2rxncon_RG_{timepstamp}" in
        the current directory. If needed script creates directory and all parent
        directories.

    -t, --total-conc:
        Include total concentration nodes for species


Output:
    Creates output directory with the RxnCon regulatory graph files in
    `gpickle`_,	`JSON node-link like`_, and DOT formats.

    To visualise the graph in PDF using `Graphviz`_ run:
        dot -x -Goverlap=scale -Tpdf Regulatory_Graph.dot > Regulatory_Graph.pdf


Examples:
    ...


References:
.. _gpickle:
    http://networkx.lanl.gov/reference/readwrite.gpickle.html
.. _JSON node-link like:
    http://networkx.github.io/documentation/latest/reference/readwrite.json_graph.html
.. _Graphviz:
    http://www.graphviz.org/


Authors:
    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>


Date: Nov 14, 2014
"""
#===============================================================================
# TODO:
#
# - Add nodes for 'components' (atm, only a node for 'component' MFalpha is
#   included)
#
# - Implement AND and OR requirements for complexes
#   (atm, species-species inlfuences analysis works like everything is an
#   (inclusive) OR requirement)
#
# - Change input format to native RxnCon CSV / XLS, including:
#	* separate input and output elemental states
#   * different contingencies for the same reaction
#===============================================================================

import datetime
import errno
import getopt
import importlib
import os
import sys

# not used, but required by nx.draw_graphviz
import matplotlib.pyplot as plt
import networkx as nx
from networkx.readwrite import json_graph


# not used, but required by nx.draw_graphviz
# auxiliary functions
def usage(err_msg=None):
    if err_msg:
        print 'Error: %s\n' % err_msg
    print "For more information run:\n\tpython rxncon2rxncon_RG.py --help\n"

def help():
    print __doc__



def mkdir_p(path, *args, **kwargs):
    # Src: http://stackoverflow.com/a/600612
    try:
        os.makedirs(path, *args, **kwargs)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


OUT_FN_PREFIX = 'Regulatory_Graph'
def out_fn(ext):
    return OUT_FN_PREFIX + '.' + ext


def now_stamp():
    return datetime.datetime.now().strftime("%y%m%d_%H%M%S")


def is_not_none(x):
    return x is not None


def get_dict_vals(d, keys):
    return list(row[k] for k in keys)


def species_str(Spec, Dom, Subdom, Res):
    return '%s%s%s%s' % (Spec, Dom, Subdom, Res)



if __name__ == '__main__':
#=========================================================================
# input args
##
#=========================================================================
    options, argv = getopt.getopt(
        sys.argv[1:], 'ho:t', ['help','output-dir=','total-conc'])

    # parse options
    out_dir = 'output_files/o_rxncon2rxncon_RG_%s' % now_stamp()
    include_tot_species = False
    for opt, arg in options:
        if opt in ('-h', '--help'):
            help()
            sys.exit(0)
        elif opt in ('-t', '--total-conc'):
            include_tot_species = True
        elif opt in ('-o', '--output-dir'):
            out_dir = arg

    # parse args
    nargs = len(argv)
    # required args
    if  nargs < 1:
        usage('Path to the Python RxnCon data dict module is required.')
        sys.exit(1)
    try:
        m_dir, m_mod = os.path.split(argv[0])
        (m_mod, _) = os.path.splitext(m_mod)
        sys.path.append(m_dir)
        o_import_rxncon = importlib.import_module(m_mod)
    except ImportError:
        usage('Cannot import the Python RxnCon data dict "%s".' % argv[0])
        sys.exit(1)




#=========================================================================
# initialize
#=========================================================================

    # prep out dir
    mkdir_p(out_dir, mode=0755)

    # init set of species and reaction nodes
    reaction_nodes = set()
    species_nodes = set()

    # specify rxncon reaction types
    interaction_reactions_names = (
        'ppi', 'i', 'BIND', 'g_ppi', 'g_i', 'g_BIND')
    phosph_reactions_names = ('GEF', 'g_GEF', 'P+', 'g_P+')
    dephosph_reactions_names = ('GAP', 'g_GAP', 'P-', 'g_P-')
    AP_reactions_names = ('AP', 'g_AP')
    PT_reactions_names = ('PT', 'g_PT')
    DEG_reactions_names = ('DEG', 'g_DEG')
    Ub_reactions_names = ('Ub+', 'g_Ub+')
    CUT_reactions_names = ('CUT', 'g_CUT')

    # input data row keys
    # transcription pseudo-reactions, w/ different format: up to 5 effectors
    conting_states_transcription_keys = (
        'ConA1', 'ConA2', 'ConA3', 'ConA4','ConB1')
    #
    conting_states_keys = conting_states_transcription_keys + \
        ('ConB2', 'ConB3', 'ConB4',)
    # transcription pseud-reaction name is under ConB2 key
    ConB2_idx = conting_states_keys.index('ConB2')
    spec_keys = ('Species', 'Domain', 'Subdomain', 'Residue')
    specA_keys = tuple("%s1" % k for k in spec_keys)
    specB_keys = tuple("%s2" % k for k in spec_keys)

    # init graph
    Regulatory_Graph = nx.DiGraph()

    # FIXME: add node for MFalpha since it is degraded
    Regulatory_Graph.add_edge(
        'MFalpha', 'MFalpha_ppi_Ste2(Receptor)', key=1, color='green')
    species_nodes.add('MFalpha')

#=========================================================================
# build graph
#=========================================================================

    for row in o_import_rxncon.rxncon_data_dict.values():

        # assign names to rows: A_reaction_B
        specA_vals = get_dict_vals(row, specA_keys)
        specB_vals = get_dict_vals(row, specB_keys)

        speciesA = species_str(*specA_vals)
        speciesB = species_str(*specB_vals)

        conting_states = filter(
            is_not_none, get_dict_vals(row, conting_states_keys))

        ContingSymbol = row['ConSymbol']
        ElementalState = row['ElementalState']
        reaction = row['Reaction']

        if reaction != '':
            reaction_node = speciesA + '_' + reaction + '_' + speciesB
            reaction_nodes.add(reaction_node)

            if include_tot_species:
                # Add species total concentration as a excplicit species node
                # and as a requirement for the reaction.
                # Total concentration contains "pure" species, which is a substrate
                # of the reaction and in rxncon it is an implicit complement of all
                # other elemental states. (Rem: for the mass conserved species it is
                # computable by the inclusion-exclusion principle)
                A = specA_vals[0]
                B = specB_vals[0]
                species_nodes.add(A)
                species_nodes.add(B)
                Regulatory_Graph.add_edge(
                    A, reaction_node, key=1, color='green')
                Regulatory_Graph.add_edge(
                    B, reaction_node, key=1, color='green')

            if ElementalState != '':

                Regulatory_Graph.add_edge(
                    reaction_node, ElementalState, key=1, color='darkgreen')
                species_nodes.add(ElementalState)

            if ContingSymbol in ('!', '!!'):

                for conting in conting_states:
                    if conting != '':

                        Regulatory_Graph.add_edge(
                            conting, reaction_node, key=1, color='green')
                        species_nodes.add(conting)

            elif ContingSymbol in ('x', 'xx'):

                for conting in conting_states:
                    if conting != '':

                        Regulatory_Graph.add_edge(
                            conting, reaction_node, key=-1, color='red', arrowtail='tee')
                        species_nodes.add(conting)

            elif ContingSymbol == '!OR':

                for conting in conting_states:
                    if conting != '':

                        Regulatory_Graph.add_edge(
                            conting, reaction_node, key=1, color='green')
                        species_nodes.add(conting)

            # differ between in/out elemental states
            if reaction in dephosph_reactions_names:

                Bph = speciesB + '-P'
                species_nodes.add(Bph)

                Regulatory_Graph.add_edge(
                    reaction_node, Bph, key=-1, color='magenta', arrowtail='tee')

            elif reaction in DEG_reactions_names:

                B = specB_vals[0]
                species_nodes.add(B)

                Regulatory_Graph.add_edge(
                    reaction_node, B, key=-1, color='magenta', arrowtail='tee')

            elif reaction in PT_reactions_names:

                Aph = speciesA + '-P'
                species_nodes.add(Aph)

                Regulatory_Graph.add_edge(
                    reaction_node, Aph, key=-1, color='magenta', arrowtail='tee')

        # transcription as pseudo-reaction
        # TODO: or species, cf. C122 ?
        elif ContingSymbol in ('!+','x+'):

            conting_states_transcription = filter(
                is_not_none, get_dict_vals(row, conting_states_transcription_keys))
            ConB2 = conting_states[ConB2_idx]
            # TODO: what are the species A and B in that case (usually equal,
            #       but not always cf. C318)?
            reaction_nodes.add(ConB2)
            edge_kwargs = {'key':1, 'color':'green'} if ContingSymbol == '!+' else  {'key':-1, 'color':'red', 'arrowtail':'tee'}
            for conting in conting_states_transcription:
                if conting != '':
                    Regulatory_Graph.add_edge(conting, ConB2, **edge_kwargs)
                    species_nodes.add(conting)


    Regulatory_Graph.add_nodes_from(species_nodes, style='filled', bipartite=0)
    Regulatory_Graph.add_nodes_from(reaction_nodes, shape='box', bipartite=1)


    # remove not connected subgraphs with only two nodes (this means isolated
    # reactions)
    Undir_Regulatory_Graph = Regulatory_Graph.to_undirected()
    connected_subgraphs = list(
        nx.connected_component_subgraphs(Undir_Regulatory_Graph))
    for graph in connected_subgraphs:
        if len(graph.nodes()) == 2:
            for node in graph.nodes():
                Regulatory_Graph.remove_node(node)

    # for u,v,d in Regulatory_Graph.edges(data=True):
    #     d['label'] = d.get('N','')
    #     d['label'] = d.get('Z','')


#=========================================================================
# export
#=========================================================================
    nx.draw_graphviz(Regulatory_Graph)

    out_dot = os.path.join(out_dir, out_fn('dot'))
    nx.write_dot(Regulatory_Graph, out_dot)

    nx.write_gpickle(
        Regulatory_Graph, os.path.join(out_dir, out_fn('gpickle')))

    try:
        output = open(os.path.join(out_dir, out_fn('json')), 'w+')
        data = json_graph.node_link_data(Regulatory_Graph)
        print >> output, data
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        sys.exit(1)
    except:
        print "Unexpected error:", sys.exc_info()[0]
        sys.exit(1)
    finally:
        output.close()

    print 'Good news, the RxnCon regulatory graph has been successfully exported.'
    out_img_ext = 'pdf'
    out_img = os.path.join(out_dir, out_fn(out_img_ext))
    print 'To plot run:'
    print '\tdot -x -Goverlap=scale -T%s %s > %s' % (out_img_ext, out_dot, out_img)
