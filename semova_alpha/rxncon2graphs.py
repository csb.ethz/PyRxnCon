#!/usr/bin/env python

"""
rxncon2graphs (previous name: export_contingencies)

Script for the translation of rxncon data into the following graphs (cf. Chapter 2 in thesis):

	- species-reaction stoichiometry graph (SRs graph), here named Reaction_Graph
	- unified species-reaction graph (SR graph), here named Species_Reaction_Graph
	- species-species inluence graph (Si graph), here named Influence_Graph

--------------------------------------------------------------------------------------------------------------------
INPUT:

1. 'rxncon_data_dict' from 'o_import_rxncon' (rxncon data)
2. 'rxncon_species_dict' from 'species_names_translation.o_species_translator' (manually translated species names)

--------------------------------------------------------------------------------------------------------------------
HOW TO RUN:

$ python rxncon2graphs.py [number of iterations]*

* default = 2

--------------------------------------------------------------------------------------------------------------------
OUTPUT: 

- graphs are stored in both gpickle and json format

[side information]
- 'o_species_rxncon2graphs': information on species, elemental states, binding states etc.
- 'o_reactions_rxncon2graphs': labels of reaction nodes (reaction identifier)

--------------------------------------------------------------------------------------------------------------------
NECESSARY FOR:

- graphs mapping (cf. Chapter 3)
- Si graph comparison (cf. Chapter 4)

--------------------------------------------------------------------------------------------------------------------
TODO:

- Split script into modules (too large and not easy to debug atm)
- name file o_rxncon2graphs automatically wrt to input file name, e.g. o_rxncon2graphs_Model_Name

--------------------------------------------------------------------------------------------------------------------
Author: JL
Date: 16/04/25
"""
import os
import sys
from networkx import DiGraph, Graph
from networkx import MultiDiGraph
from networkx import generate_edgelist, diameter, connected_component_subgraphs
from networkx import write_gpickle, average_shortest_path_length, average_clustering
from networkx.readwrite import json_graph

from species_names_translation.o_species_translator import rxncon_species_dict
from output_files.o_import_rxncon import rxncon_data_dict

import datetime

import time
start_time = time.time()

# import utilities
from utils.scripts import print_usage, now_stamp, mkdir_p, out_fn_ext


"""DEFINITION START"""

# Inititate graphs
Reaction_Graph = DiGraph()
Influence_Graph = MultiDiGraph()
Species_Reaction_Graph = MultiDiGraph()

# Init set of nodes for non-molecular modifiers, e.g. osmostress (here referred as modulators)
modulator_nodes = set()

# Init set of nodes for gene transcription
transcription_nodes = set()

# Specify rxncon reaction types
interaction_reactions_names = ['ppi', 'i', 'BIND', 'g_ppi', 'g_i', 'g_BIND']
phosph_reactions_names = ['GEF', 'g_GEF', 'P+', 'g_P+']
dephosph_reactions_names = ['GAP', 'g_GAP', 'P-', 'g_P-']
AP_reactions_names = ['AP', 'g_AP']
PT_reactions_names = ['PT', 'g_PT']
DEG_reactions_names = ['DEG', 'g_DEG']
Ub_reactions_names = ['Ub+', 'g_Ub+']
CUT_reactions_names = ['CUT', 'g_CUT']

# Init set for atomic species, i.e. not bound or modified species (here referred as pure species)
pure_species_rxncon = set()


def PositiveInfluenceInGraph(species1, species2):
	"""Def positive edges in Si graph
	"""
	global Influence_Graph

	Influence_Graph.add_edge(species1, species2, key = 1, J = 1, color = 'green')

	return Influence_Graph

def NegativeInfluenceInGraph(species1, species2):
	"""Def negative edges in Si graph
	"""
	global Influence_Graph

	Influence_Graph.add_edge(species1, species2, key = -1, J = -1, color = 'red')

	return Influence_Graph

def Reaction_SpeciesReaction_Graphs(reaction_node, enzyme, substrate, product):
	"""Def edges in SRs and SR graph
	"""
	global Species_Reaction_Graph
	global Reaction_Graph

	if product == enzyme:

		Reaction_Graph.add_edge(enzyme, reaction_node, N = 1, color = 'blue')
		Reaction_Graph.add_edge(reaction_node, enzyme, N = 2, color = 'green')
		Reaction_Graph.add_edge(substrate, reaction_node, N = 1)

		Species_Reaction_Graph.add_edge(reaction_node, enzyme, N = -1, color = 'blue')
		Species_Reaction_Graph.add_edge(reaction_node, enzyme, N = 2, color = 'darkgreen')
		Species_Reaction_Graph.add_edge(enzyme, reaction_node, Z = 1, key = 1, color = 'green')

		Species_Reaction_Graph.add_edge(reaction_node, substrate, N = -1, color = 'magenta')
		Species_Reaction_Graph.add_edge(substrate, reaction_node, Z = 1, key = 1, color = 'green')

	else:
		Reaction_Graph.add_edge(enzyme, reaction_node, N = 1, color = 'blue')
		Reaction_Graph.add_edge(reaction_node, enzyme, N = 1, color = 'blue')
		Reaction_Graph.add_edge(substrate, reaction_node, N = 1)
		Reaction_Graph.add_edge(reaction_node, product, N = 1)

		Species_Reaction_Graph.add_edge(reaction_node, enzyme, N = -1, color = 'blue')
		Species_Reaction_Graph.add_edge(reaction_node, enzyme, N = 1, color = 'blue')
		Species_Reaction_Graph.add_edge(enzyme, reaction_node, Z = 1, key = 1, color = 'green')

		Species_Reaction_Graph.add_edge(reaction_node, substrate, N = -1, color = 'magenta')
		Species_Reaction_Graph.add_edge(reaction_node, product, N = 1, color = 'darkgreen')
		Species_Reaction_Graph.add_edge(substrate, reaction_node, Z = 1, key = 1, color = 'green')

	return Species_Reaction_Graph
	return Influence_Graph

def Translate_Species_Name_A(species_raw):
	"""Translate label of species A
	"""
	global A

	if species_raw in rxncon_species_dict.keys():
		A = rxncon_species_dict[species_raw]
	else:
		A = species_raw

	return A

def Translate_Species_Name_B(species_raw):
	"""Translate label of species B
	"""
	global B

	if species_raw in rxncon_species_dict.keys():
		B = rxncon_species_dict[species_raw]
	else:
		B = species_raw

	return B

def Translate_Species_Name_AB(species_raw):
	"""Translate label of dimerized species AB (A+B->AB)
	"""
	global AB

	if species_raw in rxncon_species_dict.keys():
		AB = rxncon_species_dict[species_raw]
	else:
		AB = species_raw

	return AB

def Translate_Species_Name_A_product(species_raw):
	"""Translate label of modified species A
	"""
	global A_product

	if species_raw in rxncon_species_dict.keys():
		A_product = rxncon_species_dict[species_raw]
	else:
		A_product = species_raw

	return A_product

def Translate_Species_Name_B_product(species_raw):
	"""Translate label of modified of species B
	"""
	global B_product

	if species_raw in rxncon_species_dict.keys():
		B_product = rxncon_species_dict[species_raw]
	else:
		B_product = species_raw

	return B_product

def Translate_Species_Name_S(species_raw):
	"""Translate label of species S (involved in gene transcription)
	"""
	global S

	if species_raw in rxncon_species_dict.keys():
		S = rxncon_species_dict[species_raw]
	else:
		S = species_raw

	return S

def Check_Neg_Contingencies(line):
	"""Check wether considered reaction has a 'x' contingency.
	This check is required for rxncon reactions with both 'x' and '!' contingency.
	"""
	global neg_conting_states
	global speciesA, DomA, SubdomA, ResA, reaction, speciesB, DomB, SubdomB, ResB

	if line['Species1'] == speciesA and line['Domain1'] == DomA and line['Subdomain1'] == SubdomA and \
		line['Residue1'] == ResA and line['Reaction'] == reaction and \
		line['Species2'] == speciesB and line['Domain2'] == DomB and line['Subdomain2'] == SubdomB and \
		line['Residue2'] == ResB and line['ConSymbol'] == 'x':

		neg_conting_states_names = [line['ConA1'], line['ConA2'], line['ConA3'], \
		line['ConA4'], line['ConB1'], line['ConB2'], line['ConB3'], line['ConB4']]
		
		for conting in neg_conting_states_names:
			if conting != '':
				neg_conting_states.add(conting)

	return neg_conting_states

def Check_Modulator(line, r_node):
	"""Check wether a modulator acts on the considered reaction.
	"""
	global modulator_nodes
	global Species_Reaction_Graph
	global Influence_Graph
	global speciesA, speciesB, reaction_node, ElementalState
	global B_product, B

	if line['Species1'] == speciesA and line['Reaction'] == reaction and \
		line['Species2'] == speciesB and line['ElementalState'] == ElementalState:

	 	if line['ConSymbol'] == 'xx':

			modulator = line['ConA1']
			modulator_nodes.add(modulator)
				
			Species_Reaction_Graph.add_edge(modulator, r_node, key = -1, Z = -1, color = 'red')
			
			PositiveInfluenceInGraph(modulator, B)

			if reaction not in DEG_reactions_names:												
				NegativeInfluenceInGraph(modulator, B_product)

			#if product_states_A:
			#	NegativeInfluenceInGraph(modulator, B_product)

		if line['ConSymbol'] == '!!' or line['ConSymbol'] == '!!OR':

			modulator = line['ConA1']
			modulator_nodes.add(modulator)
				
			Species_Reaction_Graph.add_edge(modulator, r_node, key = 1, Z = 1, color = 'green')

			NegativeInfluenceInGraph(modulator, B)

			if reaction not in DEG_reactions_names:
				PositiveInfluenceInGraph(modulator, B_product)

			#if product_states_A:
			#	Influence_Graph.add_edge(modulator, A_product, J = -1, color = 'red')

	return modulator_nodes
	return Species_Reaction_Graph
	return Influence_Graph

def Generate_Interaction_Reaction():
	"""Translates rxncon interaction reactions into graphs. 
	Uses function defined above.
	"""
	global speciesA, speciesB
	global substrate_states_A, substrate_states_B
	global species_nodes_bipart, reaction_nodes_bipart
	global A, B, AB
	global Reaction_Graph, Influence_Graph, Species_Reaction_Graph
	global products

	# A
	A_raw = '+'.join(sorted(substrate_states_A))

	# B
	B_raw = '+'.join(sorted(substrate_states_B))

	A = ''
	B = ''

	Translate_Species_Name_A(A_raw)

	Translate_Species_Name_B(B_raw)

	# if A == '' or B == '':
	# 	print A, reaction, B, ElementalState

	# add species to species nodes
	species_nodes_bipart.add(A)
	species_nodes_bipart.add(B)

	# define reaction nodes
	association = A + '(' + speciesA + DomA + SubdomA + ResA + ')_as_' + B  + '(' + speciesB + DomB + SubdomB + ResB + ')'
	dissociation = A + '(' + speciesA + DomA + SubdomA + ResA + ')_dis_' + B  + '(' + speciesB + DomB + SubdomB + ResB + ')'

	mirror_association = B + '(' + speciesB + DomB + SubdomB + ResB + ')_as_' + A  + '(' + speciesA + DomA + SubdomA + ResA + ')'
	mirror_dissociation = B + '(' + speciesB + DomB + SubdomB + ResB + ')_dis_' + A  + '(' + speciesA + DomA + SubdomA + ResA + ')'

	if association not in reaction_nodes_bipart and \
		dissociation not in reaction_nodes_bipart and \
		mirror_association not in reaction_nodes_bipart and \
		mirror_dissociation not in reaction_nodes_bipart:

		# add to reaction nodes set
		reaction_nodes_bipart.add(association)
		reaction_nodes_bipart.add(dissociation)


		"""Check if a modulator acts on considered reaction"""
		for line in rxncon_data_dict.values():

			Check_Modulator(line, association)

			

		# define edges for dimerization reactions 
		if A == B and speciesA == speciesB:

			Reaction_Graph.add_edge(A, association, N = 2)
			Reaction_Graph.add_edge(dissociation, A, N = 2)


			Species_Reaction_Graph.add_edge(association, A, N = -2, color = 'magenta')
			Species_Reaction_Graph.add_edge(A, association, Z = 1, key = 1, color = 'green')

			Species_Reaction_Graph.add_edge(dissociation, A, N = 2, color = 'darkgreen')

			# redefine AB for dimerization reaction of conjoint states
			if A not in pure_species_rxncon:

				product_states_AB  = []
				product_states_AB.append(ElementalState)

				for state in substrate_states_A:
					if state not in pure_species_rxncon:
						product_states_AB.append(state)
				for state in substrate_states_B:
					if state not in pure_species_rxncon:
						product_states_AB.append(state)

				# AB
				AB_raw = '+'.join(sorted(product_states_AB))

				AB = ''

				Translate_Species_Name_AB(AB_raw)

				# if AB == '':
				# 	print AB, A, reaction, B, ElementalState

				# add species to species nodes
				species_nodes_bipart.add(AB)
				products_append(product_states_AB)


				Reaction_Graph.add_edge(association, AB, N = 1)
				Reaction_Graph.add_edge(AB, dissociation, N = 1)

				Species_Reaction_Graph.add_edge(association, AB, N = 1, color = 'darkgreen')
				Species_Reaction_Graph.add_edge(dissociation, AB, N = -1, color = 'magenta')
				Species_Reaction_Graph.add_edge(AB, dissociation, Z = 1, key = 1, color = 'green')

				PositiveInfluenceInGraph(A, AB)								
				PositiveInfluenceInGraph(AB, A)
			
			# AB = elemental state for dimerization of pure species
			else:

				product_states_AB = []

				for state in sorted(substrate_states_A):

					if state not in pure_species_rxncon:

						product_states_AB.append(state)

				for state in sorted(substrate_states_B):

					if state not in pure_species_rxncon:

						product_states_AB.append(state)

				product_states_AB.append(ElementalState)


				# AB
				AB_raw = '+'.join(sorted(product_states_AB))

				AB = ''

				Translate_Species_Name_AB(AB_raw)

				# if AB == '':
				# 	print AB, A, reaction, B, ElementalState
				
				species_nodes_bipart.add(AB)
				products_append(product_states_AB)

				Reaction_Graph.add_edge(association, AB, N = 1)
				Reaction_Graph.add_edge(AB, dissociation, N = 1)

				Species_Reaction_Graph.add_edge(association, AB, N = 1, color = 'darkgreen')
				Species_Reaction_Graph.add_edge(dissociation, AB, N = -1, color = 'magenta')
				Species_Reaction_Graph.add_edge(AB, dissociation, Z = 1, key = 1, color = 'green')


				PositiveInfluenceInGraph(A, AB)
				PositiveInfluenceInGraph(AB, A)

				##print 'Define reaction for ', A, '+', B, '->', AB

		# additional ppi in a protein complex already formed (e.g. Fus3-Ste5 Unlock)
		elif A == B and speciesA != speciesB:

			# Define product states AB
			product_states_AB = []

			for state in sorted(substrate_states_A):

				if state not in pure_species_rxncon:

					product_states_AB.append(state)

			product_states_AB.append(ElementalState)


			# AB
			AB_raw = '+'.join(sorted(product_states_AB))

			AB = ''

			Translate_Species_Name_AB(AB_raw)

			# if AB == '':
			# 	print AB, A, reaction, B, ElementalState

			species_nodes_bipart.add(AB)
			products_append(product_states_AB)

			Reaction_Graph.add_edge(A, association, N = 1)
			Reaction_Graph.add_edge(dissociation, A, N = 1)

			Species_Reaction_Graph.add_edge(association, A, N = -1, color = 'magenta')
			Species_Reaction_Graph.add_edge(A, association, Z = 1, key = 1, color = 'green')

			Species_Reaction_Graph.add_edge(dissociation, A, N = 1, color = 'darkgreen')


			product_states_AB  = []
			product_states_AB.append(ElementalState)


			# edge1 = (association, AB)
			# edge2 = (AB, dissociation)

			#interaction_reactions_bipart.append(edge1)
			#interaction_reactions_bipart.append(edge2) 

			Reaction_Graph.add_edge(association, AB, N = 1)
			Reaction_Graph.add_edge(AB, dissociation, N = 1)

			Species_Reaction_Graph.add_edge(association, AB, N = 1, color = 'darkgreen')
			Species_Reaction_Graph.add_edge(dissociation, AB, N = -1, color = 'magenta')
			Species_Reaction_Graph.add_edge(AB, dissociation, Z = 1, key = 1, color = 'green')

			PositiveInfluenceInGraph(A, AB)								
			PositiveInfluenceInGraph(AB, A)
			

			#print 'Define reaction for ', A, '+', B, '->', AB


		else:

			# Define product states AB
			product_states_AB = []

			for state in sorted(substrate_states_A):

				if state not in pure_species_rxncon:

					product_states_AB.append(state)

			for state in sorted(substrate_states_B):

				if state not in pure_species_rxncon:

					product_states_AB.append(state)

			product_states_AB.append(ElementalState)


			# AB
			AB_raw = '+'.join(sorted(product_states_AB))

			AB = ''

			Translate_Species_Name_AB(AB_raw)

			# if AB == '':
			# 	print AB, A, reaction, B, ElementalState
			
			species_nodes_bipart.add(AB)
			products_append(product_states_AB)

			Reaction_Graph.add_edge(A, association, N = 1)
			Reaction_Graph.add_edge(B, association, N = 1)
			Reaction_Graph.add_edge(association, AB, N = 1)
			Reaction_Graph.add_edge(AB, dissociation, N = 1)
			Reaction_Graph.add_edge(dissociation, A, N = 1)
			Reaction_Graph.add_edge(dissociation, B, N = 1)


			Species_Reaction_Graph.add_edge(association, A, N = -1, color = 'magenta')
			Species_Reaction_Graph.add_edge(association, B, N = -1, color = 'magenta')
			Species_Reaction_Graph.add_edge(association, AB, N = 1, color = 'darkgreen')

			Species_Reaction_Graph.add_edge(dissociation, AB, N = -1, color = 'magenta')
			Species_Reaction_Graph.add_edge(dissociation, A, N = 1, color = 'darkgreen')
			Species_Reaction_Graph.add_edge(dissociation, B, N = 1, color = 'darkgreen')

			Species_Reaction_Graph.add_edge(A, association, Z = 1, key = 1, color = 'green')
			Species_Reaction_Graph.add_edge(B, association, Z = 1, key = 1, color = 'green')
			Species_Reaction_Graph.add_edge(AB, dissociation, Z = 1, key = 1, color = 'green')



			PositiveInfluenceInGraph(A, AB)
			PositiveInfluenceInGraph(B, AB)
			PositiveInfluenceInGraph(AB, A)
			PositiveInfluenceInGraph(AB, B)
			NegativeInfluenceInGraph(A, B)
			NegativeInfluenceInGraph(B, A)

			##print 'Define reaction for ', A, '+', B, '->', AB

def Generate_Dephospho_Reaction():
	"""Translates rxncon dephosphorylation reactions (P-, GAP) into graphs. 
	Uses function defined above.
	"""

	global speciesB
	global substrate_states_A, substrate_states_B
	global species_nodes_bipart, reaction_nodes_bipart
	global A, B, B_product, A_product, S, pureB_phoph
	global Reaction_Graph, Influence_Graph, Species_Reaction_Graph
	global products

	# Define edges for enzymatic reactions
	"""Define substrate names for species A and species B"""
	# rename variables A, B and using elemental states in substrate_states_A/B
	# A
	A_raw = '+'.join(sorted(substrate_states_A))

	# B
	B_raw = '+'.join(sorted(substrate_states_B))

	A = ''
	B = ''

	Translate_Species_Name_A(A_raw)
	Translate_Species_Name_B(B_raw)

	# if B == '' or A == '':
	# 	print A, reaction, B, ElementalState
	
	# add species to species nodes
	species_nodes_bipart.add(A)
	species_nodes_bipart.add(B)

	"""Define product states and reaction nodes."""
	# Define product state of B
	product_states_B = []

	
	for state in sorted(substrate_states_B):
		product_states_B.append(state)
				
	# replace phosphorylated state of B with unphosphorylated state
	product_states_B.remove(pureB_phoph)
	
	if not product_states_B:
		product_states_B.append(speciesB)

	B_product_raw = '+'.join(sorted(product_states_B))

	B_product = ''

	Translate_Species_Name_B_product(B_product_raw)

	if reaction not in DEG_reactions_names:
		species_nodes_bipart.add(B_product)
		products_append(product_states_B)


	"""define reaction node"""
	reaction_node = A + '(' + speciesA + DomA + SubdomA + ResA + ')_' + reaction + '_' + B  + '(' + speciesB + DomB + SubdomB + ResB + ')'
	
	if reaction_node not in reaction_nodes_bipart:

		# add reaction node to reaction_nodes
		reaction_nodes_bipart.add(reaction_node)

		# check if modulator is acting on considered reaction
		for line in rxncon_data_dict.values():

			Check_Modulator(line, reaction_node)


		if A == B: 

			edge1 = (B, reaction_node)
			edge2 = (reaction_node, B_product)

			Reaction_Graph.add_edge(B, reaction_node, N = 1, color = 'red')
			Reaction_Graph.add_edge(reaction_node, B_product, N = 1)

			Species_Reaction_Graph.add_edge(reaction_node, B, N = -1, color = 'magenta')
			Species_Reaction_Graph.add_edge(reaction_node, B_product, N = 1, color = 'darkgreen')
			Species_Reaction_Graph.add_edge(B, reaction_node, Z = 1, key = 1, color = 'green')

			PositiveInfluenceInGraph(B, B_product)

		else:

			Reaction_SpeciesReaction_Graphs(reaction_node, A, B, B_product)
			
			PositiveInfluenceInGraph(B, B_product)
			PositiveInfluenceInGraph(A, B_product)
			NegativeInfluenceInGraph(A, B)

def Generate_Modification_Reaction():
	"""Translates rxncon modification reactions into graphs which are not a dephosphorylation reaction. 
	Uses function defined above.
	"""
	global substrate_states_A, substrate_states_B
	global species_nodes_bipart, reaction_nodes_bipart
	global A, B, B_product, A_product, S
	global Reaction_Graph, Influence_Graph, Species_Reaction_Graph
	global products
	global speciesA, speciesB

	# Define edges for enzymatic reactions
	"""Define substrate names for species A and species B"""
	# rename variables A, B and using elemental states in substrate_states_A/B
	# A
	A_raw = '+'.join(sorted(substrate_states_A))

	# B
	B_raw = '+'.join(sorted(substrate_states_B))

	A = ''
	B = ''

	Translate_Species_Name_A(A_raw)
	Translate_Species_Name_B(B_raw)
	
	# add species to species nodes
	# if B == '' or A == '':
	# 	print A, reaction, B, ElementalState

	species_nodes_bipart.add(A)
	species_nodes_bipart.add(B)

	"""Define product states and reaction nodes."""
	# Define product state of B
	product_states_B = []
	product_states_A = []

	# Phosphotransfer reaction: both A and B change the state, A-P + B -> A + B-P
	if reaction in PT_reactions_names:

		pureA_phoph = speciesA + DomA + SubdomA + ResA + '-P'

		# B product state as for modification reactions
		product_states_B.append(ElementalState)

		for state in sorted(substrate_states_B):
			if state not in pure_species_rxncon:
				product_states_B.append(state)

		if A == B:
			product_states_B.remove(pureA_phoph)
		
		# A product state as for dephosphorylation reactions												
		for state in sorted(substrate_states_A):
			product_states_A.append(state)
		
		# replace phosphorylated state of A with unphosphorylated state
		product_states_A.remove(pureA_phoph)
		
		if not product_states_A:
			pure_A = speciesA
			product_states_A.append(pure_A)

		for state in product_states_B:
			if state == '':
				product_states_B.remove(state)
		for state in product_states_A:
			if state == '':
				product_states_A.remove(state)

	else:
		if reaction not in DEG_reactions_names:

			product_states_B.append(ElementalState)

			for state in substrate_states_B:
				if state not in pure_species_rxncon:
					product_states_B.append(state)


			for state in product_states_B:
				if state == '':
					product_states_B.remove(state)
			#print product_states_B


			B_product_raw = '+'.join(sorted(product_states_B))

			B_product = ''

			Translate_Species_Name_B_product(B_product_raw)

			species_nodes_bipart.add(B_product)
			products_append(product_states_B)
		

	# if product_states_A created, define name of product state of A
	if product_states_A:
		A_product_raw = '+'.join(sorted(product_states_A))

		A_product = ''

		Translate_Species_Name_A_product(A_product_raw)


		species_nodes_bipart.add(A_product)
		#species_nodes_wo_combis.add(A_product)
		products_append(product_states_A)

	"""define reaction node"""
	reaction_node = A + '(' + speciesA + DomA + SubdomA + ResA + ')_' + reaction + '_' + B  + '(' + speciesB + DomB + SubdomB + ResB + ')'
	
	if reaction_node not in reaction_nodes_bipart:

		# add reaction node to reaction_nodes
		reaction_nodes_bipart.add(reaction_node)

		# check if modulator is acting on considered reaction
		for line in rxncon_data_dict.values():

			Check_Modulator(line, reaction_node)


		"""Phosphorylation etc. reactions."""
		if reaction in phosph_reactions_names or reaction in Ub_reactions_names or reaction in CUT_reactions_names:

			if A == B: 

				Reaction_Graph.add_edge(B, reaction_node, N = 1, color = 'red')
				Reaction_Graph.add_edge(reaction_node, B_product, N = 1)

				Species_Reaction_Graph.add_edge(reaction_node, B, N = -1, color = 'magenta')
				Species_Reaction_Graph.add_edge(reaction_node, B_product, N = 1, color = 'darkgreen')
				Species_Reaction_Graph.add_edge(B, reaction_node, Z = 1, key = 1, color = 'green')

				PositiveInfluenceInGraph(B, B_product)

			else:

				Reaction_SpeciesReaction_Graphs(reaction_node, A, B, B_product)
				
				PositiveInfluenceInGraph(B, B_product)
				PositiveInfluenceInGraph(A, B_product)
				NegativeInfluenceInGraph(A, B)

		

		"""Degradation reaction."""

									
		if reaction in DEG_reactions_names:

			if len(substrate_states_B) == 1 and B_raw not in binding_states:

				#print 77777777777777777777777, A
				#print B_raw


				Reaction_Graph.add_edge(A, reaction_node, N = 1, color = 'blue')
				Reaction_Graph.add_edge(reaction_node, A, N = 1, color = 'blue')
				Reaction_Graph.add_edge(B, reaction_node, N = 1)

				Species_Reaction_Graph.add_edge(reaction_node, A, N = 1, color = 'darkgreen')
				Species_Reaction_Graph.add_edge(reaction_node, A, N = -1, color = 'magenta')
				Species_Reaction_Graph.add_edge(A, reaction_node, Z = 1, color = 'green')

				Species_Reaction_Graph.add_edge(reaction_node, B, N = -1, color = 'magenta')
				Species_Reaction_Graph.add_edge(B, reaction_node, Z = 1, color = 'green')

				NegativeInfluenceInGraph(A, B)




			elif B_raw not in pure_species_rxncon:

				# print 77777777777777777777777777777777777, A
				# print B_raw

				product_states = []

				# returns product states
				for state in substrate_states_B:

					if state not in speciesB_elemental_states:
						product_states.append(state)

					if state in binding_states:

						for key in dic_of_species_states.keys():
							if key != speciesB:
								if state in dic_of_species_states[key]:
									product_states.append(key)
				#print product_states
				# remove pure species if not the single state
				for state in product_states:
					if state in pure_species_rxncon and len(product_states) != 1:
						product_states.remove(state)

				for state in product_states:
					if state in pure_species_rxncon and len(product_states) != 1:
						product_states.remove(state)

				#print product_states

				Remaining_raw = ''
				i = 0
				for state in sorted(product_states):
					if i < 1:
						Remaining_raw = state 
						i +=1
					else: Remaining_raw += '+' + state

				if Remaining_raw in rxncon_species_dict.keys():

					Remaining = rxncon_species_dict[Remaining_raw]
				else:
					Remaining = Remaining_raw

				#print Remaining

				Reaction_Graph.add_edge(A, reaction_node, N = 1, color = 'blue')
				Reaction_Graph.add_edge(reaction_node, A, N = 1, color = 'blue')
				Reaction_Graph.add_edge(B, reaction_node, N = 1)
				Reaction_Graph.add_edge(reaction_node, Remaining, N = 1)

				Species_Reaction_Graph.add_edge(reaction_node, A, N = -1, color = 'magenta')
				Species_Reaction_Graph.add_edge(reaction_node, A, N = 1, color = 'darkgreen')
				Species_Reaction_Graph.add_edge(A, reaction_node, Z = 1, color = 'green')

				Species_Reaction_Graph.add_edge(reaction_node, B, N = -1, color = 'magenta')
				Species_Reaction_Graph.add_edge(reaction_node, Remaining, N = 1, color = 'darkgreen')
				Species_Reaction_Graph.add_edge(B, reaction_node, Z = 1, color = 'green')


				NegativeInfluenceInGraph(A, B)
				PositiveInfluenceInGraph(B, Remaining)
				PositiveInfluenceInGraph(A, Remaining)

				species_nodes_bipart.add(Remaining)




		"""Autophosphorylation reaction."""
		if reaction in AP_reactions_names:

			# edge1 = (B, reaction_node)
			# edge2 = (reaction_node, B_product)

			# AP_reactions_bipart.append(edge1)
			# AP_reactions_bipart.append(edge2)

			Reaction_Graph.add_edge(B, reaction_node, N = 1)
			Reaction_Graph.add_edge(reaction_node, B_product, N = 1)

			Species_Reaction_Graph.add_edge(reaction_node, B, N = -1, color = 'magenta')
			Species_Reaction_Graph.add_edge(reaction_node, B_product, N = 1, color = 'darkgreen')
			Species_Reaction_Graph.add_edge(B, reaction_node, Z = 1, key = 1, color = 'green')

			PositiveInfluenceInGraph(B, B_product)

			#print reaction
			#print B, ' -> ', B_product

		"""Phosphotransfer reaction."""	
		if reaction in PT_reactions_names:
			# edges for species-reaction graph

			if A == B:

				Reaction_Graph.add_edge(B, reaction_node, N = 1, color = 'red')
				Reaction_Graph.add_edge(reaction_node, B_product, N = 1)

				Species_Reaction_Graph.add_edge(reaction_node, B, N = -1, color = 'magenta')
				Species_Reaction_Graph.add_edge(reaction_node, B_product, N = 1, color = 'darkgreen')
				Species_Reaction_Graph.add_edge(B, reaction_node, Z = 1, key = 1, color = 'green')

				PositiveInfluenceInGraph(B, B_product)

				#print reaction
				#print  B, ' -> ', B_product


			else:

				Reaction_SpeciesReaction_Graphs(reaction_node, A, B, B_product)


				PositiveInfluenceInGraph(B, B_product)
				PositiveInfluenceInGraph(A, B_product)
				NegativeInfluenceInGraph(A, B)
				PositiveInfluenceInGraph(A, A_product)
				PositiveInfluenceInGraph(B, A_product)
				NegativeInfluenceInGraph(B, A)

				#print reaction
				#print A, ' + ', B, ' -> ', A_product, ' + ', B_product


"""store atomic (here: pure) species"""
for row in rxncon_data_dict.values():
	if row['Species1'] != '':
 	# add atomic species to species set
		pure_species_rxncon.add(row['Species1'])
	if row['Species2'] != '':
		pure_species_rxncon.add(row['Species2'])

# initiate dict with all states of all species
dic_of_species_states = {}

# init set for every atomic species
for species in pure_species_rxncon:

	dic_of_species_states[species] = set()

# store all elemental states
all_elemental_states = set()

# init set of binding (intersection) states
binding_states = set()

"""Create dict for each species with all states: pure states and elemental states"""
for row in rxncon_data_dict.values():

	# assign names to rows (better readable): A_reaction_B -> elem_state
	A = row['Species1']
	B = row['Species2']

	reaction = row['Reaction'] 
	elem_state = row['ElementalState']

	dic_of_species_states[A].add(A)
	dic_of_species_states[B].add(B)

	if reaction in interaction_reactions_names:
		dic_of_species_states[A].add(elem_state)
		dic_of_species_states[B].add(elem_state)
		
		binding_states.add(elem_state)
		all_elemental_states.add(elem_state)

	else:
		if elem_state != '':
			dic_of_species_states[B].add(elem_state)
			all_elemental_states.add(elem_state)


"""Initiate set for bipartite nodes: species and reactions"""
reaction_nodes_bipart = set()

species_nodes_bipart = set()


def Generate_Reaction_Edges_and_Products(row):
	"""Main graph building function. Uses functions defined above.
	Generates reactions between species according to rxncon semantics.
	"""

	global speciesA, DomA, SubdomA, ResA, reaction, speciesB, DomB, SubdomB, ResB, ElementalState
	global A, B, AB, A_product, B_product, S, pureB_phoph

	global neg_conting_states

	global products
	global allowed_species_in_system
	global Influence_Graph
	global Species_Reaction_Graph
	global Reaction_Graph
	global reaction_nodes_bipart
	global species_nodes_bipart
	global modulator_nodes
	global species_nodes_wo_combis

	global substrate_states_A, substrate_states_B, product_states_AB

	global speciesA_elemental_states, speciesB_elemental_states

	# assign names to rows: A_reaction_B
	speciesA, A = row['Species1'], row['Species1']
	DomA = row['Domain1'] 
	SubdomA = row['Subdomain1'] 
	ResA = row['Residue1'] 

	speciesB, B = row['Species2'], row['Species2']
	DomB = row['Domain2'] 
	SubdomB = row['Subdomain2'] 
	ResB = row['Residue2']

	ConA1 = row['ConA1']
	ConA2 = row['ConA2']
	ConA3 = row['ConA3']
	ConA4 = row['ConA4']

	ConB1 = row['ConB1']
	ConB2 = row['ConB2']
	ConB3 = row['ConB3']
	ConB4 = row['ConB4']


	ContingSymbol = row['ConSymbol']
	ElementalState = row['ElementalState']
	reaction = row['Reaction'] 


	# call set with all elemental states of considered species A and B, required for deepness check
	speciesA_elemental_states = dic_of_species_states[A]
	speciesB_elemental_states = dic_of_species_states[B]

	#print '###############################################################################################################################'
	#print 'Determine edges for:'
	#print '###### REACTION ######', A , reaction, B, ';' , ContingSymbol, ConA1, ConA2, ConA3, ConA4, ConB1, ConB2, ConB3
	#print 'Output of reaction:', ElementalState


	"""I) w/o contingency: Reaction for all possible substrate combinations of A and B"""
	# if no contingency:
	if ContingSymbol == '-' or ContingSymbol == '!!OR':

		"""For all substrate combinations of species A and species B"""

		if reaction in interaction_reactions_names:

			substrate_states_A = []
			substrate_states_B = []

			for combination_A in allowed_species_in_system:
				##print combination_A

				if ElementalState not in combination_A and set(combination_A) & speciesA_elemental_states:

					substrate_states_A = []

					for state in combination_A:

						substrate_states_A.append(state)
					##print substrate_states_A

			
					for combination_B in allowed_species_in_system:
						##print combination_B

						if ElementalState not in combination_B and set(combination_B) & speciesB_elemental_states:

							substrate_states_B = []

							for state in combination_B:

								substrate_states_B.append(state)

							#print substrate_states_A, substrate_states_B
							Generate_Interaction_Reaction()
			
		else:

			if reaction in dephosph_reactions_names:
				
				pureB_phoph = speciesB + DomB + SubdomB + ResB + '-P'

			for combination_A in allowed_species_in_system:

				if set(combination_A) & speciesA_elemental_states:

					substrate_states_A = []

					for state in combination_A:

						substrate_states_A.append(state)

				
					for combination_B in allowed_species_in_system:

						"""Dephosphorylation reaction"""
						if reaction in dephosph_reactions_names:

							if pureB_phoph in combination_B:

								substrate_states_B = []

								for state in combination_B:

									substrate_states_B.append(state)


								Generate_Dephospho_Reaction()



						else:

							if ElementalState not in combination_B and set(combination_B) & speciesB_elemental_states:

								substrate_states_B = []

								for state in combination_B:

									substrate_states_B.append(state)

								Generate_Modification_Reaction()


	"""II) '!' Contingency: Reaction only between states, where all required states are present (incl. deepness check) """
	# if '!' contingency:
	if ContingSymbol == '!' or ContingSymbol == '!OR':

		speciesA = A
		speciesB = B

		combination_A = []
		combination_B = []

		#print '! Contingency: '

		"""Save effector states"""
		# list of effector states for A: required for iteration
		conting_states_speciesA = set()
		ConA_names = [ConA1, ConA2, ConA3, ConA4]
		# add to list only if ConState exists
		for conting in ConA_names:
			if conting != '':
				conting_states_speciesA.add(conting)
		conting_states_speciesA.discard(None)
		##print conting_states_speciesA

		# list of effector states for B: required for iteration
		conting_states_speciesB = set()
		ConB_names = [ConB1, ConB2, ConB3, ConB4]
		# add to list only if ConState exists
		for conting in ConB_names:
			if conting != '':	
				conting_states_speciesB.add(conting)
		conting_states_speciesB.discard(None)
		##print conting_states_speciesB

		if reaction in dephosph_reactions_names:

			pureB_phoph = speciesB + DomB + SubdomB + ResB + '-P'

			conting_states_speciesB.add(pureB_phoph)


		# Check if considered reaction also has 'x' contingency:
		neg_conting_states = set()
		for line in rxncon_data_dict.values():

			Check_Neg_Contingencies(line)
		
		neg_conting_states.discard(None)


		#print 'Define reaction edges...'
		if reaction in interaction_reactions_names: 	

			for combination_A_check in allowed_species_in_system:
			
				if speciesA_elemental_states & set(combination_A_check) and not speciesB_elemental_states & set(combination_A_check):

					if conting_states_speciesA <= set(combination_A_check) and \
						ElementalState not in combination_A_check and not neg_conting_states & set(combination_A_check):

						combination_A = combination_A_check

				
				elif speciesA_elemental_states & set(combination_A_check) and speciesB_elemental_states & set(combination_A_check):

					if conting_states_speciesA <= set(combination_A_check) and conting_states_speciesB <= set(combination_A_check) and \
						ElementalState not in combination_A_check and not neg_conting_states & set(combination_A_check):

						combination_A = combination_A_check


				if len(combination_A) != 0:

					substrate_states_A = []
					for state in combination_A:
						substrate_states_A.append(state) 

					for combination_B_check in allowed_species_in_system:

						if speciesB_elemental_states & set(combination_B_check) and not speciesA_elemental_states & set(combination_B_check):

							if conting_states_speciesB <= set(combination_B_check) and \
								ElementalState not in combination_B_check and not neg_conting_states & set(combination_B_check):

								combination_B = combination_B_check

						
						elif speciesB_elemental_states & set(combination_B_check) and speciesA_elemental_states & set(combination_B_check):

							if conting_states_speciesA <= set(combination_B_check) and conting_states_speciesB <= set(combination_B_check) and \
								ElementalState not in combination_B_check and not neg_conting_states & set(combination_B_check):

								combination_B = combination_B_check


						if len(combination_B) != 0:

							substrate_states_B = []
							for state in combination_B: 
								substrate_states_B.append(state)

							Generate_Interaction_Reaction()
				   		
		else:

			for combination_A_check in allowed_species_in_system:
			
				if speciesA_elemental_states & set(combination_A_check) and not speciesB_elemental_states & set(combination_A_check):

					if conting_states_speciesA <= set(combination_A_check) and \
						ElementalState not in combination_A_check and not neg_conting_states & set(combination_A_check):

						combination_A = combination_A_check

				
				elif speciesA_elemental_states & set(combination_A_check) and speciesB_elemental_states & set(combination_A_check):

					if conting_states_speciesA <= set(combination_A_check) and conting_states_speciesB <= set(combination_A_check) and \
						ElementalState not in combination_A_check and not neg_conting_states & set(combination_A_check):

						combination_A = combination_A_check


				if len(combination_A) != 0:

					substrate_states_A = []
					for state in combination_A:
						substrate_states_A.append(state) 

					for combination_B_check in allowed_species_in_system:

						if speciesB_elemental_states & set(combination_B_check) and not speciesA_elemental_states & set(combination_B_check):

							if conting_states_speciesB <= set(combination_B_check) and \
								ElementalState not in combination_B_check and not neg_conting_states & set(combination_B_check):

								combination_B = combination_B_check

						
						elif speciesB_elemental_states & set(combination_B_check) and speciesA_elemental_states & set(combination_B_check):

							if conting_states_speciesA <= set(combination_B_check) and conting_states_speciesB <= set(combination_B_check) and \
								ElementalState not in combination_B_check and not neg_conting_states & set(combination_B_check):

								combination_B = combination_B_check

						
						if len(combination_B) != 0:

							substrate_states_B = []
							for state in combination_B: 
								substrate_states_B.append(state)

							"""Dephosphorylation reaction"""
							if reaction in dephosph_reactions_names:

								if pureB_phoph in combination_B:

									substrate_states_B = []

									for state in combination_B:

										substrate_states_B.append(state)

									Generate_Dephospho_Reaction()

							else:

								substrate_states_B = []

								for state in combination_B:

									substrate_states_B.append(state)

								Generate_Modification_Reaction()


	"""III) 'x' Contingency: Reaction only between states w/o any effector state"""
	if ContingSymbol == 'x':

		check_conting = 0

		for line in rxncon_data_dict.values():

			if line['Species1'] == speciesA and line['Domain1'] == DomA and line['Subdomain1'] == SubdomA and \
				line['Residue1'] == ResA and line['Reaction'] == reaction and \
				line['Species2'] == speciesB and line['Domain2'] == DomB and line['Subdomain2'] == SubdomB and \
				line['Residue2'] == ResB and line['ConSymbol'] == '!':

				check_conting += 1

		if check_conting == 0:

			#print 'Define reaction edges for x Contingency...'

			"""Save effector states"""
			# list of effector states for A: required for iteration
			conting_states_species = set()
			ConA_names = [ConA1, ConA2, ConA3, ConA4, ConB1, ConB2, ConB3, ConB4]
			# add to list only if ConState exists
			for conting in ConA_names:
				if conting != '':
					conting_states_species.add(conting)
			conting_states_species.discard(None)
			##print conting_states_species


			#print 'Define reaction edges...'

			if reaction in interaction_reactions_names:

				for combination_A in allowed_species_in_system:

					if not conting_states_species & set(combination_A) and ElementalState not in combination_A and \
						set(combination_A) & speciesA_elemental_states:	
						
						substrate_states_A = []
						for state in combination_A:
							substrate_states_A.append(state) 

						for combination_B in allowed_species_in_system:

							if not conting_states_species & set(combination_B) and ElementalState not in combination_B and \
								set(combination_B) & speciesB_elemental_states:
								
								substrate_states_B = []
								for state in combination_B: 
									substrate_states_B.append(state)


								"""Add nodes and edges for interaction reactions"""
		
								Generate_Interaction_Reaction()

					
			else:

				if reaction in dephosph_reactions_names:
					
				    pureB_phoph = speciesB + DomB + SubdomB + ResB + '-P'

				for combination_A in allowed_species_in_system:

					if not conting_states_species & set(combination_A) and ElementalState not in combination_A and \
						set(combination_A) & speciesA_elemental_states:	
						
						substrate_states_A = []
						for state in combination_A:
							substrate_states_A.append(state) 

						for combination_B in allowed_species_in_system:

							if not conting_states_species & set(combination_B) and ElementalState not in combination_B and \
								set(combination_B) & speciesB_elemental_states:
								
								"""Dephosphorylation reaction"""
								if reaction in dephosph_reactions_names:

									if pureB_phoph in combination_B:

										substrate_states_B = []

										for state in combination_B:

											substrate_states_B.append(state)

										Generate_Dephospho_Reaction()


								else:

									substrate_states_B = []

									for state in combination_B:

										substrate_states_B.append(state)

									Generate_Modification_Reaction()


	"""IV) Transcription """
	if ContingSymbol == '!+':

		transcription = ConB2
		transcription_nodes.add(transcription)

		reaction_node = transcription + '_activation'


		conting_states = set()
		Con_names = [ConA1, ConA2, ConA3, ConA4, ConB1]
		# add to list only if ConState exists
		for conting in Con_names:
			if conting != '':	
				conting_states.add(conting)

		conting_states.discard(None)
		#print conting_states

		if reaction_node not in reaction_nodes_bipart:

			Reaction_Graph.add_edge(reaction_node, transcription, N = 1)
			Species_Reaction_Graph.add_edge(reaction_node, transcription, N = 1, color = 'darkgreen')
			reaction_nodes_bipart.add(reaction_node)
	
		for combination in allowed_species_in_system:

			if conting_states <= set(combination) and set(combination) & speciesA_elemental_states or \
				conting_states <= set(combination) and set(combination) & speciesB_elemental_states:

				substrate_states = []

				for state in combination:
					if state != '':
						substrate_states.append(state)

				S_raw = '+'.join(sorted(substrate_states))

				S = ''

				Translate_Species_Name_S(S_raw)

				species_nodes_bipart.add(S)

				Species_Reaction_Graph.add_edge(S, reaction_node, Z = 1, key = 1, color = 'green')

				PositiveInfluenceInGraph(S, transcription)

	elif ContingSymbol == 'x+':

		transcription = ConB2
		transcription_nodes.add(transcription)

		reaction_node = transcription + '_activation'

		reaction_nodes_bipart.add(reaction_node)

		conting_states = set()
		Con_names = [ConA1, ConA2, ConA3, ConA4, ConB1]
		# add to list only if ConState exists
		for conting in Con_names:
			if conting != '':	
				conting_states.add(conting)

		conting_states.discard(None)


		if reaction_node not in reaction_nodes_bipart:
			Reaction_Graph.add_edge(reaction_node, transcription, N = 1)
			Species_Reaction_Graph.add_edge(reaction_node, transcription, N = 1, color = 'darkgreen')
			reaction_nodes_bipart.add(reaction_node)
	
		for combination in allowed_species_in_system:

			if conting_states <= set(combination) and set(combination) & speciesA_elemental_states or \
				conting_states <= set(combination) and set(combination) & speciesB_elemental_states:
				#print combination

				#print combination
				substrate_states = []

				for state in combination:
					if state != '':
						substrate_states.append(state)

				S_raw = '+'.join(sorted(substrate_states))

				S = ''

				Translate_Species_Name_S(S_raw)


				species_nodes_bipart.add(S)

				Reaction_Graph.add_edge(reaction_node, transcription, N = 1)

				Species_Reaction_Graph.add_edge(S, reaction_node, Z = -1, key = -1, color = 'red')

				NegativeInfluenceInGraph(S, transcription)


	return products
	return allowed_species_in_system
	return Influence_Graph
	return Species_Reaction_Graph
	return Reaction_Graph
	return reaction_nodes_bipart
	return species_nodes_bipart
	return modulator_nodes
	return species_nodes_wo_combis


atomic_substrate_species = set() #[]
allowed_species_in_system = set() #[]

for species in pure_species_rxncon:
	atomic_substrate_species.add((species,))


for species in atomic_substrate_species:
	allowed_species_in_system.add(tuple(sorted(species)))


def Contingecies_Iterator():
	for row in rxncon_data_dict.values():
		Generate_Reaction_Edges_and_Products(row)

def Def_New_Substrates():
	print "products length: %d" % len(products)
#	print products
	for species in products:
	    Add_Or_Ignore_Species(species)

def Add_Or_Ignore_Species(species):
	global products_counter
	global allowed_species_in_system
	sspec = tuple(sorted(species))
	if  sspec not in allowed_species_in_system:
		products_counter.add(tuple(species))
		allowed_species_in_system.add(sspec)

def products_append(l):
	global products
	products.add(tuple(l))


def Print_Summary():
	"""print summary for the builded graphs
	"""
	print '----------------------------------------------------------------------------------------------------'
	print '# rounds:', count
	print '# final allowed species in system:', len(allowed_species_in_system)
	print '# new products in last round:', len(products_counter)
	print '----------------------------------------------------------------------------------------------------'
	print '----------------------------------------------------------------------------------------------------'
	print '# species nodes in total:', len(species_nodes_bipart) #len(Influence_Graph.nodes())
	print '# reactions nodes in total:', len(reaction_nodes_bipart)
	print '-----------------------------------------------------------------------------------------------------'

def Print_Species_Info():
	output_species = open('output_files/o_rxncon2graphs/o_species.py', 'w+')

	print >> output_species, '# species =', len(species_nodes_bipart)
	print >> output_species, 'pure_species_rxncon =', pure_species_rxncon 
	print >> output_species, 'species_nodes_bipart =', species_nodes_bipart 
	print >> output_species, 'binding_states =', binding_states
	print >> output_species, 'all_elemental_states=', all_elemental_states

def Print_Reactions_Info():
	output_reactions = open('output_files/o_rxncon2graphs/o_reactions.py', 'w+')

	print >> output_reactions, '# reactions =', len(reaction_nodes_bipart)	
	print >> output_reactions, 'reaction_nodes_bipart =', reaction_nodes_bipart

def Suppl_Graph_Info():
	Reaction_Graph.add_nodes_from(species_nodes_bipart, style='filled',bipartite=0)
	Reaction_Graph.add_nodes_from(reaction_nodes_bipart, shape='box', bipartite=1)

	Species_Reaction_Graph.add_nodes_from(species_nodes_bipart, style='filled',bipartite=0)
	Species_Reaction_Graph.add_nodes_from(reaction_nodes_bipart, shape='box', bipartite=1)

	for u,v,d in Reaction_Graph.edges(data=True):
	    d['label'] = d.get('N','')


	for u,v,d in Influence_Graph.edges(data=True):
	    d['label'] = d.get('J','')


	for u,v,d in Species_Reaction_Graph.edges(data=True):
	    if 'N' in d:
	       d['label'] = d.get('N','')
	    if 'Z' in d:
	       d['label'] = d.get('Z','') 

def Print_Edges():
	"""Prints graph edges, useful for debugging.
	"""
	output13 = open('output_files/o_rxncon2graphs/o_edges_Stoichiometry_Graph.py', 'w+')
	output14 = open('output_files/o_rxncon2graphs/o_edges_Species_Reaction_Graph.py', 'w+')
	output15 = open('output_files/o_rxncon2graphs/o_edges_Influence_Graph.py', 'w+')

	for line in generate_edgelist(Reaction_Graph, delimiter= ', ', data=True):
		print >> output13, line

	for line in generate_edgelist(Species_Reaction_Graph, delimiter= ', ', data=True):
		print >> output14, line

	for line in generate_edgelist(Influence_Graph, delimiter= ', ', data=True):
		print >> output15, line

def Write_gpickle_Graphs():
	write_gpickle(Reaction_Graph, 'output_files/o_rxncon2graphs/o_ReactionGraph_rxncon.gpickle')
	write_gpickle(Species_Reaction_Graph, 'output_files/o_rxncon2graphs/o_SpeciesReactionGraph_rxncon.gpickle')
	write_gpickle(Influence_Graph, 'output_files/o_rxncon2graphs/o_InfluenceGraph_rxncon.gpickle')

def Export_json_Graphs():
	outputG1 = open('output_files/o_rxncon2graphs/o_Data_Influence_Graph.json', 'w+')
	outputG2 = open('output_files/o_rxncon2graphs/o_Data_Reaction_Graph.json', 'w+')
	outputG3 = open('output_files/o_rxncon2graphs/o_Data_Species_Reaction_Graph.json', 'w+')

	data1 = json_graph.node_link_data(Influence_Graph)
	print >> outputG1, data1

	data2 = json_graph.node_link_data(Reaction_Graph)
	print >> outputG2, data2

	data3 = json_graph.node_link_data(Species_Reaction_Graph)
	print >> outputG3, data3

def Print_Graph_Info():
	Influence_Graph_simple = Graph(Influence_Graph)
	Influence_Graph_undir = Influence_Graph_simple.to_undirected()
	Influence_Graph_Subgraphs = connected_component_subgraphs(Influence_Graph_undir)

	for graph in Influence_Graph_Subgraphs:
		print 'diameter', diameter(graph)
		print 'CPL', average_shortest_path_length(graph)
		print 'avC', average_clustering(graph)

def Print_Modul_and_Transcr():
	if len(transcription_nodes) != 0 or len(modulator_nodes) != 0 :
		output10 = open('output_files/o_rxncon2graphs/o_transcription_and_modulators.py', 'w+')
		print >> output10, 'transcription_nodes =', transcription_nodes
		print >> output10, 'modulator_nodes =', modulator_nodes

def Print_Dict_Species_States():
	output12 = open('output_files/o_rxncon2graphs/o_dic_of_species_states.py', 'w+')
	print >> output12, 'dic_of_species_states =', dic_of_species_states

def Write_dot_for_Plotting():
	import networkx as nx

	nx.draw_graphviz(Species_Reaction_Graph)
	nx.write_dot(Species_Reaction_Graph, 'output_files/o_rxncon2graphs/o_graph_pics/SR.dot')

	nx.draw_graphviz(Influence_Graph)
	nx.write_dot(Influence_Graph, 'output_files/o_rxncon2graphs/o_graph_pics/Si.dot')

	nx.draw_graphviz(Reaction_Graph)
	nx.write_dot(Reaction_Graph, 'output_files/o_rxncon2graphs/o_graph_pics/SRs.dot')

	print 'For plotting Si/SRs/SR graphs, run:\n'
	print '$ dot -x -Goverlap=scale -Tpdf output_files/o_rxncon2graphs/o_graph_pics/Si.dot > output_files/o_rxncon2graphs/o_graph_pics/out_Si.pdf \n'	
	print '$ dot -x -Goverlap=scale -Tpdf output_files/o_rxncon2graphs/o_graph_pics/SRs.dot > output_files/o_rxncon2graphs/o_graph_pics/out_SRs.pdf \n'
	print '$ dot -x -Goverlap=scale -Tpdf output_files/o_rxncon2graphs/o_graph_pics/SR.dot > output_files/o_rxncon2graphs/o_graph_pics/out_SR.pdf \n'

"""DEFINITION END"""

products_counter = [1]

# set default number for iteration steps
steps = 2
# iteration step counter
count = 0


if __name__ == '__main__':
    
    if len(sys.argv) > 1:
        steps = int(sys.argv[1])


	#while len(products_counter) != 0:
	while count < steps:
		count += 1

		products = set() #[]
		products_counter = set() #[]

		Contingecies_Iterator()

		Def_New_Substrates()

	out_dir = 'output_files/o_rxncon2graphs' #%s' % now_stamp()
	out_pics = 'output_files/o_rxncon2graphs/o_graph_pics'
	mkdir_p(out_dir)
	mkdir_p(out_pics)

	Print_Summary()

	Print_Species_Info()
	Print_Reactions_Info()

	Print_Modul_and_Transcr()

	Print_Dict_Species_States()

	Suppl_Graph_Info() 

	Write_gpickle_Graphs()

	Print_Edges()

	print("--- %s seconds ---" % str(time.time() - start_time))

	Export_json_Graphs()

	"""(un)comment if necessary"""
	#Print_Graph_Info()

	"""(un)comment if necessary"""
	Write_dot_for_Plotting()

	init = open('output_files/o_rxncon2graphs/__init__.py', 'w')




#print len(pure_species_rxncon)

#diff = set(allowed_species_in_system) - set(species_nodes_bipart)
#print diff
# for s in diff:
# 	print s