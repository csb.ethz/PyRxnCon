#!/usr/bin/env python

"""
import_rxncon

Script for the import of rxncon data. Stores the rxncon information in a dicitonary.

-----------------------------------------------------------------------------------------------
INPUT:

1. Rxncon data in csv format. 
	(Modify one of the csv files in the 'input_files' folder if necessary.)
	the csv-file of the rxncon data needs to have a special structure, 
	i.e. the following column order:
	- ID
	- Output (elemental) state
	- SpeciesA
	- DomainA
	- SubdomainA
	- ResidueA
	- Reaction type
	- SpeciesA
	- DomainA
	- SubdomainA 
	- ResidueA
	- Contingency symbol
	- Effector state 1 for SpeciesA
	- Effector state 2 for SpeciesA
	- Effector state 3 for SpeciesA
	- Effector state 4 for SpeciesA
	- Effector state 1 for SpeciesB
	- Effector state 2 for SpeciesB
	- Effector state 3 for SpeciesB
	- Effector state 4 for SpeciesB
    ________
	Example:

		R1,Ste2--Ste2,Ste2,TMD,,,ppi,Ste2,TMD,,,-,,,,,,,,  
		R2,Sst2(S539)-P,Fus3,KD,,,P+,Sst2,,,S539,!,Fus3(T180)-P,Fus3(Y182)-P,,,,,,
		R3,Mkk1(S377)-P,Bck1,KD,,,P+,Mkk1,,,S377,!,Bck1-Mkk1,,,,Bck1-Mkk1,,,


2. List of species names (strings!) to specify which contingencies has to be imported
    ________
	Example: ['MFalpha,'Ste2','Bar1'] 

-----------------------------------------------------------------------------------------------
HOW TO RUN:

$ python import_rxncon.py [list of species names] [csv file]
    ________
	Example: 

	$ python import_rxncon.py ['MFalpha','Ste2','Bar1'] input_files/rxncon_data_for_import-m.csv

To import all continegencies, run:

	$ python import_rxncon.py all [csv file]

-----------------------------------------------------------------------------------------------
OUTPUT: 

Dictionary 'rxncon_data_dict'

-----------------------------------------------------------------------------------------------
NECESSARY FOR:

- 'rxncon2graphs.py'

-----------------------------------------------------------------------------------------------
TODO:

- Make the csv file structure more convenient

-----------------------------------------------------------------------------------------------
Author: JL
Date: 15/03/10
"""

import sys
from csv import DictReader

# init dictionary for rxncon data
rxncon_data_dict = {}

# List of keys for the rxncon data dictionary
header_list = ['Number', \
				'ElementalState', \
				'Species1', \
				'Domain1', \
				'Subdomain1', \
				'Residue1', \
				'Reaction', \
				'Species2', \
				'Domain2', \
				'Subdomain2', \
				'Residue2', \
				'ConSymbol',\
				'ConA1', \
				'ConA2', \
				'ConA3', \
				'ConA4', \
				'ConB1', \
				'ConB2', \
				'ConB3', \
				'ConB4']


def Import_Contingencies(row):
	"""Imports rxncon information and 
	selects contingencies for the species defined in input.
	"""

	global rxncon_data_dict

	# put Domain, Subdomain and Residue names in brackets
	for key in row.keys():
		a = key
		if row[a] != '':

			if a == 'Domain1' or a == 'Domain2' or \
			a == 'Subdomain1' or a == 'Subdomain2' or \
			a == 'Residue1' or a == 'Residue2':
				row[key] = '(' + row[a] + ')'

	ID = row['Number']

	if str(sys.argv[1]) == 'all':
		"""Import all contingencies
		"""

		rxncon_data_dict[ID] = row


	else:
		"""Import only contingencies which comprise considered species
		"""

		consided_species = sys.argv[1]
		A = row['Species1'] 
		B = row['Species2']

		if A in consided_species and B in consided_species:
			rxncon_data_dict[ID] = row
	    
	return rxncon_data_dict



if __name__ == '__main__':

	if len(sys.argv) == 3:

		csv_file = sys.argv[2]

		rxncon_data = open(csv_file, 'r')

		reactions = DictReader(rxncon_data, header_list)


		for row in reactions:
			Import_Contingencies(row)

		output = open('output_files/o_import_rxncon.py', 'w+')

		print >> output, '##############################################'
		print >> output, '## Imported contingencies as a latex table: ##'
		print >> output, '##############################################'
		print >> output, "'''"
		for row in rxncon_data_dict.values():
			print >> output, row['Species1'], '&', row['Reaction'], '&', row['Species2'],'&', \
			row['ConSymbol'], '&', row['ConA1'], '&', row['ConA2'], '&', row['ConA3'], '&', row['ConA3'],'&', \
			row['ConB1'], '&', row['ConB2'], '&', row['ConB3'], '&', row['ConB4'], '\ '.strip() + '\ '
		print >> output, "'''"

		print >> output, '############################'
		print >> output, '## Rxncon data dictionary ##'
		print >> output, '############################'
		print >> output, 'rxncon_data_dict = ', rxncon_data_dict

		# Print summary
		if str(sys.argv[1]) == 'all':

			print 'All contingencies imported! \n\nNEXT STEP: rxncon2graphs.py'

		else:
			print 'Imported contingencies:\n'

			for row in rxncon_data_dict.values():
				print row['Species1'], row['Reaction'], row['Species2'], \
				row['ConSymbol'], row['ConA1'], row['ConA2'], row['ConA3'], row['ConA3'], \
				row['ConB1'], row['ConB2'], row['ConB3'], row['ConB4']
			print '\nNEXT STEP: rxncon2graphs.py'

	else:

		print 'ERROR! Specify species and csv file!'
		print "Example: \n$ python import_rxncon.py ['MFalpha','Ste2','Bar1'] input_files/rxncon_data_for_import-m.csv"
	

