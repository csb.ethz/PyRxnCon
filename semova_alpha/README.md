LAST UPDATE: 2016/04/25

Author: Janina Linnik <janina.linnik@bsse.ethz.ch>

Current state of the documented code for the semi-automatic model validation.

**NOTE: This version uses an outdated rxncon input format - this will be updated in the next version.**
**Further future changes include:**

* new rxncon data import

* new rxncon regulatory graph representation

* new rxncon regulatory graph comparison (*compare_RGs.py*)

* SBML import and SBML export to SRs and Si graphs

* comparison of SRs/Si graphs
 

## FILE STRUCTURE:

	- input_files -> rxncon data in csv-format

	- import_rxncon.py (for the import of rxncon data)
	- rxncon2graphs.py (for the export of rxncon data to species reaction and species influence graphs)
	- rxncon2rxncon_RG.py (for the export of rxncon data to the rxncon regulatory graph)

	- species_names_translation -> auxiliary scripts for the species mapping

	- output_files -> graphs in json and gpickle format,
	- o_graph_pics -> dot files for png picture export



## INSTALLATION

Install the following python libraries:

 * networkx
 
 * matplotlib
 
 * pygraphviz (requires graphviz)



## EXAMPLE: How to export to rxncon data to graphs?

Brief example that shows how to export rxncon data semi-automatically to 

    - species-reaction stoichiometry graph (SRs)
    - species influence graph (Si)
    - unified species-reaction graph (SR)
    - rxncon regulatory graph (RG, to be changed to eSR in the next update)

Detailed information about each step can be found in the docstrings.
Execute from the semova_alpha directory.

  **Import rxncon data:**
   
    python import_rxncon.py all input_files/example.csv   

 **Translate rxncon data into**

  a. SRs, Si, SR graphs 
 (the integer denotes the number of iterations to construct the SRs graph from rxncon data):

    python rxncon2graphs.py 5

  b. rxncon regulatory graph:

    python rxncon2rxncon_RG.py -o output_files/o_example output_files/o_import_rxncon.py

  Optionally: Plot graphs according to displayed instruction.
  
