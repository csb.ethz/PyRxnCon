#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    compare_RGs - compare species-species influences in two RxnCon regulatory
        graphs.


Usage:
    python compare_RGs.py gpickle_path_1 gpickle_path_2


Attributes:
    gpickle_path_{1,2}:
        Paths to RxnCon regulatory graph `gpickle`_ files. Each file is an
        output of the `rxncon2rxncon_RG.py` script. Two graphs are required for
        comparison. Bigger or first of the graphs is called the data graph, the
        other graph is called the model graph.


Options:
    -h, --help:
        Print this info.

    -o PATH, --output-dir=PATH:
        Output directory path, by default " o_compareRGs_{timepstamp}" in
        the current directory. If needed script creates directory and all parent
        directories.

    -p, --print:
        print in


.. _gpickle:
    http://networkx.lanl.gov/reference/readwrite.gpickle.html


Authors:
    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>


Date: Nov 14, 2014
"""

import datetime
import errno
import getopt
import importlib
import os
import sys
import networkx as nx
from networkx.readwrite import json_graph
from networkx import bipartite

# not used, but required by nx.draw_graphviz
import matplotlib.pyplot as plt


# auxiliary functions
def usage(err_msg=None):
    if err_msg:
        print 'Error: %s\n' % err_msg
    print "For more information run:\n\tpython compare_RGs.py --help\n"

def help():
    print __doc__


def mkdir_p(path, *args, **kwargs):
    # Src: http://stackoverflow.com/a/600612
    try:
        os.makedirs(path, *args, **kwargs)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def del_empty_val(dic):
    for key in dic.keys():
        if len(dic[key]) == 0: del dic[key]

def store_RG_inf(G, node1, node2, dic):
    """
    Parses influences in G from node1 to node2
    and stores sign of influences in a dictionary,
    e.g., dic[(node1,node2)] = set([+1,-1]).
    """
    if nx.has_path(G, node1, node2):
        # init dic entry
        dic[(node1, node2)] = set()
                        # check direct and indirect influences
        for path in nx.all_simple_paths(G, node1, node2):
            if len(path) > 1:
                # if s1 == 'Hog1(Y176)-P' and s2 == s1: # print loop
                    # print path
                # init Jacobian label
                k = 1
                # init index for nodes in path
                n = 0
                while (n+1) < len(path):
                    k = k*G[path[n]][path[n+1]]['key']
                    #print RG_m[path[n]][path[n+1]]#['key']
                    n += 1

                if k < 0:
                    dic[(node1, node2)].add(-1)
                if k > 0:
                    dic[(node1, node2)].add(1)
    return dic

def merge_nodes(G, nodes, new_node, attr_dict=None, **attr):
    """
    Merges the selected 'nodes' of the graph G into one 'new_node'.
    Adjacent edges of 'nodes' became adjacent edges of the 'new_node'.
    attr_dict and **attr are defined as in 'G.add_node'.
    """

    G.add_node(new_node, attr_dict, **attr) # Add the 'merged' node

    for n1,n2,d in G.edges(data=True):
        d['c'] = d.get('color','')
        d['k'] = d.get('key','')
        d['a'] = d.get('arrowhead','')

        if n1 in nodes and n2 in nodes:
            G.add_edge(new_node,new_node, color = d['c'], key = d['k'], arrowhead = d['a'])
        elif n1 in nodes:
            G.add_edge(new_node,n2, color = d['c'], key = d['k'], arrowhead = d['a'])
        elif n2 in nodes:
            G.add_edge(n1,new_node, color = d['c'], key = d['k'], arrowhead = d['a'])


    for n in nodes: # remove the merged nodes
        G.remove_node(n)


def out_fn(OUT_FN_PREFIX, ext):
    return OUT_FN_PREFIX + '.' + ext


def now_stamp():
    return datetime.datetime.now().strftime("%y%m%d_%H%M%S")

def get_graphviz_attr(G, attr):
    for u,v,d in G.edges(data=True):
        d[attr] = d.get(attr,'')

if __name__ == '__main__':
#=========================================================================
# input args
#=========================================================================
    options, argv = getopt.getopt(
        sys.argv[1:], 'ho:p', ['help','output-dir=','print'])

    # parse options
    out_dir = 'output_files/o_compare_RGs_%s' % now_stamp()
    print_sum = False
    for opt, arg in options:
        if opt in ('-h', '--help'):
            help()
            sys.exit(0)
        elif opt in ('-p', '--print'):
            print_sum = True
        elif opt in ('-o', '--output-dir'):
            out_dir = arg

    nargs = len(argv)

    # required args
    if  nargs < 2:
        usage('Path to two regulatory graph gpickle files is required.')
        sys.exit(1)
    path1, path2 = argv[:2]


#=========================================================================
# initialize
#=========================================================================

    # import RGs
    try:
        RG1 = nx.read_gpickle(path1)
        RG2 = nx.read_gpickle(path2)
    except ImportError:
        usage('Cannot import the Python Regulatory Graphs from "%s" and/or from "%s".' %(path1, path2))
        sys.exit(1)

    # prep out dir
    mkdir_p(out_dir, mode=0755)

    # assign model and data RG to RG1, RG2
    RG_d, RG_m = (RG1, RG2) if len(RG1) >= len(RG2) else (RG2, RG1)

    # assign reaction nodes and species nodes to sets
    S_nodes_m, R_nodes_m = bipartite.sets(RG_m)
    #FIXME: modified Tiger graph is not bipartite
    #S_nodes_d, S_nodes_d = bipartite.sets(RG_d)
    nodes_d = set(RG_d.nodes())

    # determine intersection of species states in model and data
    considered_S_nodes = nodes_d.intersection(S_nodes_m)
    # TODO: implement dictionary to UNLUMP species states
    # e.g., Ste7-P in model might correspond to Ste7(AL)(S359)-P AND/OR Ste7(AL)(T363)-P in data

    # not identified model species states

    not_identified_S_nodes = S_nodes_m - considered_S_nodes

    if print_sum == True:
        print '\nConsidered species nodes:\n(compare influences only between these species nodes)'
        n = 0
        for s in considered_S_nodes:
            n += 1
            print '\t %d %s' % (n,s)

    # init dict for sign of graph paths
    influences_m = {}
    influences_d = {}

    # determine sign of graph paths:
    # keys are tuples of a pair of species states
    # values are sets with elements of {1, -1}, i.e. pos. and/or negative path
    # e.g., (stateA, stateB): set([1])

    for s1 in considered_S_nodes:
        for s2 in considered_S_nodes:
            #if s1 != s2:

            # parse model RG
            store_RG_inf(RG_m, s1, s2, influences_m)
            del_empty_val(influences_m)

            # parse data RG
            store_RG_inf(RG_d, s1, s2, influences_d)
            del_empty_val(influences_d)

    # print results for model regulatory graph

    if print_sum == True:
        # FIXME: MULTIPLE PRINTING...???
        print "Species state influences in model regulatory graph: "
        for k,v in influences_m.items():
            for i in k: print '\t  %s' % i
            for i in v: print '\t %s' % i
            print "-------------------------"
        print "End of 'Species state influences in model regulatory graph' "

        print "Species state influences in data regulatory graph: "
        for k,v in influences_d.items():
            for i in k: print '\t  %s' % i
            for i in v: print '\t %s' % i
            print "-------------------------"
        print "End of 'Species state influences in data regulatory graph' "

        if not_identified_S_nodes:
            print 'Not identified species states in model:'
            for s in not_identified_S_nodes:
                print '\t %s' % s

    # init networkx graphs and dicts for:
    # a) influences in data but NOT IN MODEL
    # b) influences in model but NOT IN DATA
    Inf_Not_in_Model_subgraph = nx.MultiDiGraph()
    Inf_Not_in_Data_subgraph = nx.MultiDiGraph()
    Inf_Not_in_Model_dict = {}
    Inf_Not_in_Data_dict = {}


#=========================================================================
# compare graphs (TODO: IMPROVE LOGIC)
#=========================================================================

    match_count_m = 0
    match_count_d = 0

    # TODO: Def function for RGs comparison
    for key in influences_m.keys():

        if key in influences_d.keys():

            inf_m = influences_m[key]
            inf_d = influences_d[key]

            match = inf_m.intersection(inf_d)
            match_count_m += len(match)

            if inf_m != inf_d:
                diff_inf = inf_m - match
                Inf_Not_in_Data_dict[key] = diff_inf

                #Inf_Not_in_Data_subgraph.add_nodes_from(key, style = 'filled')

                for inf in Inf_Not_in_Data_dict[key]:

                    if inf > 0:
                        Inf_Not_in_Data_subgraph.add_edge(key[0], key[1], color = 'green', key=1, arrowhead='vee')
                    if inf < 0:
                        Inf_Not_in_Data_subgraph.add_edge(key[0], key[1], color = 'red', key=-1, arrowhead='tee')


        else:
            Inf_Not_in_Data_dict[key] = influences_m[key]

            #Inf_Not_in_Data_subgraph.add_nodes_from(key, style = 'filled')

            for inf in Inf_Not_in_Data_dict[key]:

                if inf > 0:
                    Inf_Not_in_Data_subgraph.add_edge(key[0], key[1], color = 'green', key=1, arrowhead='vee')
                if inf < 0:
                    Inf_Not_in_Data_subgraph.add_edge(key[0], key[1], color = 'red', key=-1, arrowhead='tee')


    for key in influences_d.keys():

        if key[0] in RG_m.nodes() or key[1] in RG_m.nodes():

            if key in influences_m.keys():

                inf_m = influences_m[key]
                inf_d = influences_d[key]

                match = inf_m.intersection(inf_d)
                match_count_d += len(match)

                if inf_m != inf_d:
                    diff_inf = inf_d - match
                    Inf_Not_in_Model_dict[key] = diff_inf

                    #Inf_Not_in_Data_subgraph.add_nodes_from(key, style = 'filled')

                    for inf in Inf_Not_in_Model_dict[key]:

                        Inf_Not_in_Model_subgraph.add_node(key[0], color = 'blue')
                        Inf_Not_in_Model_subgraph.add_node(key[1], color = 'blue')

                        if inf > 0:
                            Inf_Not_in_Model_subgraph.add_edge(key[0], key[1], color = 'green', key=1, arrowhead='vee')
                        if inf < 0:
                            Inf_Not_in_Model_subgraph.add_edge(key[0], key[1], color = 'red', key=-1, arrowhead='tee')

            else:
                Inf_Not_in_Model_dict[key] = influences_d[key]

                for inf in Inf_Not_in_Model_dict[key]:

                    Inf_Not_in_Model_subgraph.add_node(key[0], color = 'blue')
                    Inf_Not_in_Model_subgraph.add_node(key[1], color = 'blue')

                    if inf > 0:
                        Inf_Not_in_Model_subgraph.add_edge(key[0], key[1], color = 'green', key=1, arrowhead='vee')
                    if inf < 0:
                        Inf_Not_in_Model_subgraph.add_edge(key[0], key[1], color = 'red', key=-1, arrowhead='tee')

    if print_sum == True:

        print 'Missing influences in model:'
        for key in Inf_Not_in_Model_dict.keys():
            print key, Inf_Not_in_Model_dict[key]

        print '\nNumber of matching influences:\n'
        print '\tmatch_count_m:', match_count_m
        print '\tmatch_count_d:', match_count_d
        if match_count_d == match_count_m:
            print '\t\tSame number -> OK!\n'


#=========================================================================
# export
#=========================================================================

    # get graphviz edge attributes (NOT WORKING, TO FIX!) - @MR: does it work now?
    get_graphviz_attr(Inf_Not_in_Data_subgraph, 'arrowhead')
    get_graphviz_attr(Inf_Not_in_Model_subgraph, 'arrowhead')

    nx.draw_graphviz(Inf_Not_in_Data_subgraph)
    nx.draw_graphviz(Inf_Not_in_Model_subgraph)

    # TODO: improve def of out_fn() function
    out_dot_data = os.path.join(out_dir, out_fn('Inf_Not_in_Data', 'dot'))
    nx.write_dot(Inf_Not_in_Data_subgraph, out_dot_data)

    out_dot_model = os.path.join(out_dir, out_fn('Inf_Not_in_Model', 'dot'))
    nx.write_dot(Inf_Not_in_Model_subgraph, out_dot_model)

    print 'Completed! Regulatory Graphs successfully compared.'
    out_img_ext = 'pdf'
    out_img_data = os.path.join(out_dir, out_fn('Inf_Not_in_Data', out_img_ext))
    out_img_model = os.path.join(out_dir, out_fn('Inf_Not_in_Model', out_img_ext))

    print 'To plot results as Regulatory Graph subgraphs run:'
    print '\tdot -x -Goverlap=scale -T%s "%s" > "%s"' % (out_img_ext, out_dot_data, out_img_data)
    print '\n\tdot -x -Goverlap=scale -T%s "%s" > "%s"' % (out_img_ext, out_dot_model, out_img_model)




#=========================================================================
# adjust results to model granularity
# has to be done manually
# TODO: to make it semi-automatic, create dictionary
# that maps certain species states to a set of elemental states,
# eg., Hog1-PP to {Hog1(T174)-P, Hog1(Y176)-P}
#=========================================================================

    # # # init graphs
    # Inf_Not_in_Data_adjust = Inf_Not_in_Data_subgraph.copy()
    # Inf_Not_in_Model_adjust = Inf_Not_in_Model_subgraph.copy()

    # # # define nodes to merge
    # adjust_Hog1pp = ('Hog1(T174)-P', 'Hog1(Y176)-P')
    # adjust_Fus3pp = ('Fus3(T180)-P', 'Fus3(Y182)-P')
    # adjust_Ste11ppp = ('Ste11(CBD)(S302)-P', 'Ste11(CBD)(S306)-P', 'Ste11(CBD)(T307)-P')

    # merge_nodes(Inf_Not_in_Model_adjust, adjust_Ste11ppp, 'Ste11-PPP', color = 'blue')

    # merge_nodes(Inf_Not_in_Data_adjust, adjust_Fus3pp, 'Fus3-PP')

    # merge_nodes(Inf_Not_in_Data_adjust, adjust_Hog1pp, 'Hog1-PP')

    # get_graphviz_attr(Inf_Not_in_Data_adjust, 'shape')
    # get_graphviz_attr(Inf_Not_in_Model_adjust, 'shape')

    # nx.draw_graphviz(Inf_Not_in_Data_adjust)
    # nx.draw_graphviz(Inf_Not_in_Model_adjust)

    # out_dot_data_adj = os.path.join(out_dir, out_fn('Inf_Not_in_Data_adjust', 'dot'))
    # nx.write_dot(Inf_Not_in_Data_adjust, out_dot_data_adj)

    # out_dot_model_adj = os.path.join(out_dir, out_fn('Inf_Not_in_Model_adjust', 'dot'))
    # nx.write_dot(Inf_Not_in_Model_adjust, out_dot_model_adj)

    # out_img_data_adj = os.path.join(out_dir, out_fn('Inf_Not_in_Data_adjust', out_img_ext))
    # out_img_model_adj = os.path.join(out_dir, out_fn('Inf_Not_in_Model_adjust', out_img_ext))

    # print '\n\tdot -x -Goverlap=scale -T%s %s > %s |' % (out_img_ext, out_dot_data_adj, out_img_data_adj)
    # print '\n\tdot -x -Goverlap=scale -T%s %s > %s' % (out_img_ext, out_dot_model_adj, out_img_model_adj)




