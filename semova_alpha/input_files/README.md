## **NOTE: This input format is outdated and will be changed in the next update.**
## IN THIS FOLDER

- rxncon_data_for_import-tiger.csv (rxncon data version 1, *Tiget et al., Molecular Systems Biology, 2012*)
- rxncon_data_for_import-m.csv (rxncon data version 2, *Flöttmann et al., BMC Systems Biology, 2013*)
- example.csv (rxncon data toy example)
- McClean_rxncon_fixed+gran_adjust.csv (translated ODE model, taken from *McClean et al., Nature genetics, 2007*) 
## RxnCon Data Format (outdated, to be updated)
*.csv* file with the following column structure:

Column 1:  ID

Column 2:  Output elemental state

Column 3:  SpeciesA

Column 4:  DomainA

Column 5:  SubdomainA

Column 6:  ResidueA

Column 7:  Reaction type

Column 8:  SpeciesB

Column 9:  DomainB

Column 10: SubdomainB

Column 11: ResidueB

Column 12: Contingency symbol

Column 13: Effector state 1 for SpeciesA

Column 14: Effector state 2 for SpeciesA

Column 15: Effector state 3 for SpeciesA

Column 16: Effector state 4 for SpeciesA

Column 17: Effector state 1 for SpeciesB

Column 18: Effector state 2 for SpeciesB

Column 19: Effector state 3 for SpeciesB

Column 20: Effector state 4 for SpeciesB
        
		________
    	Example:

		R1,Ste2--Ste2,Ste2,TMD,,,ppi,Ste2,TMD,,,-,,,,,,,,
		
		R2,Sst2(S539)-P,Fus3,KD,,,P+,Sst2,,,S539,!,Fus3(T180)-P,Fus3(Y182)-P,,,,,,
		
		R3,Mkk1(S377)-P,Bck1,KD,,,P+,Mkk1,,,S377,!,Bck1-Mkk1,,,,Bck1-Mkk1,,,
