PyRxnCon Tests
================

PyRxnCon unit tests.


Run
---

In the tests directory, run from the command-line:


     python -m unittest discover
     python -m unittest discover -v
     python -m unittest test_graphs
     python -m unittest -v test_graphs
     python -m unittest -v test_graphs.TestESRGraph
     
See [Python unittest docs on CLI](https://docs.python.org/2/library/unittest.html#command-line-interface) for more info.


Contact
-------

    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>
