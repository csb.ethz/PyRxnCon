# -*- coding: utf-8 -*-
'''
Created on Nov 4, 2015

@author: Mikołaj Rybiński <mikolaj.rybinski@gmail.com>
'''
import unittest
import sys
import networkx


class TestESRGraph(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

################################################################################
# Tests TODO:
# * graph for each single reaction
# * enzymatic reaaction (few variants)
#
# Rem: graphs equality by nieghbours list equality, assuming equality of labels
#      + also test crucial kwargs like edge sign; what else?
#



# ################################################################################
# # Examples from Python docs
# #
#     def test_upper(self):
#         self.assertEqual('foo'.upper(), 'FOO')
# 
# # basic
#     def test_isupper(self):
#         self.assertTrue('FOO'.isupper())
#         self.assertFalse('Foo'.isupper())
# 
#     def test_split(self):
#         s = 'hello world'
#         self.assertEqual(s.split(), ['hello', 'world'])
#         # check that s.split fails when the separator is not a string
#         with self.assertRaises(TypeError):
#             s.split(2)
# 
# # skipping
#     @unittest.skip("demonstrating skipping")
#     def test_nothing(self):
#         self.fail("shouldn't happen")
# 
#     @unittest.skipIf(networkx.__version__ < (1, 9),
#                      "not supported in this library version")
#     def test_old_lib(self):
#         # Tests that work for only a certain version of the library.
#         pass
# 
#     @unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
#     def test_windows_support(self):
#         # windows specific testing code
#         pass
# 
# # expected failure
#     @unittest.expectedFailure
#     def test_fail(self):
#         self.assertEqual(1, 0, "broken")



################################################################################
# Direct call off (see README.md); uncomment if necessary.
# if __name__ == "__main__":
#     # import sys;sys.argv = ['', 'Test.testName']
#     unittest.main()
#
