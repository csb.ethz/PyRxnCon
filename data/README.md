RxnCon Data
===========

Data and models in the RxnCon format, i.e. a list of elemental reactions,
a list of elemental species states contingencies for these reactions, and an
optional lists of booleans to define complex elemental species effectors.



Folder contents
---------------

[Tiger XLS]:
    `Tiger_et_al-TableS1-140513.xls` - first paper data possibly with few
    corrections; obtained from Marcus Krantz on 13 May 2014.

[Flöttman XLS]:
    `Floettmann_et_al-TableS5-140905.xls` - boolean modelling paper data (with
    transcription nodes); obtained from Marcus Krantz on 5 Sep 2014.

Folders containig at least sample models and data subset definitions in the
RxnCon format (cf. `README` files therin).



Data format
-----------

RxnCon contains a reactions list, contingencies list, and optional booleans
lists.


### Basic ###

1. The (unique) elemental *reaction key* is a string consisting of:

        ${Species A key}_${Reaction type}_${Species B key}

2. *Species key* is a string build as:

        ${Species}[${Domain}/${Subdomain}(${Residue})]

    where each empty variable part is omitted, including the separators
    (i.e. `[]`, `/`, `()`).

3. Elemental reaction changes one or both *elemental states* of reactants;
   these are strings representing either:

    - directly species, produced by the "synthesis/degradation" reactions
      (e.g. `DEG`):

    	    ${Species key}

      or
    - binding, produced by the association reactions (e.g. `ppi`):

    	    ${Species key}--${Species key}

      or
    - modifications, consumed or produced by the covalent modification reactions
      (e.g. `P+`, `P-`, `Ub+`, `CUT`):

	        ${Species key}-{${Modification}}

      Modification string is one of `P`, `U`, and `Truncated`.

    *Remark*: elemental reaction types from categories of "relocalisation" and
    "metabolic conversion" are not currently supported.



### Elemental reactions ###

Each elemental reaction is in the following format:

    ${Reaction ID},
    ${Species A}, ${Domain A}, ${Subdomain A}, ${Residue A},
    ${Reaction type},
    ${Species B}, ${Domain B}, ${Subdomain B}, ${Residue B},

Each elemental reaction can be uniquely defined by the reaction key.

*Remark*: if reaction has only a genetic evidence, then the reaction type
is preceeded with the prefix `g:`.

Each elemental reaction has at least one of source or a product elemental
states. These are automatically constructed according to the
"Reaction definition" templates (cf. [Tiger XLS]).

#### Example ####

The following reactions:

    R1,Bck1,,,,ppi,Mkk1,,,,
    R2,Bck1,KD,,,P+,Mkk1,,,S377
    R3,ukPPase43,,,,P-,Mkk1,,,S377

have the following unique reaction keys (preceeded with ID):

    R1,Bck1_ppi_Mkk1
    R2,Bck1[KD]_P+_Mkk1[(S377)]
    R3,ukPPase43_P-_Mkk1[(S377)]

and the following source and product elemental states (preceeded with ID):

    R1,,Bck1--Mkk1
    R2,,Mkk1[(S377)]-{P}
    R3,Mkk1[(S377)]-{P},



### Booleans ###

Elemental states can be combined via `AND` or `OR` boolean gates to define a
complex effector states - intersections or unions of species elemental states.
Each boolean is defined by the operator and a list of terms compromising it.
Each boolean is in the following format:

    ${Boolean ID},
    ${Boolean operator}

and each boolean term is in the following format:

    ${Boolean term ID},
    ${Boolean ID},
    Effector

Boolean ID is written in angle brackets `<...>`. For two or more boolean terms
of the same boolean the effectors are combined according to the boolean
operator, i.e. via `AND` or via `OR`.

Effectors in boolean terms are the same as in contingencies (elemental states,
other booleans, or system inputs).

*Remark*: the multi-level booleans can always be reduced to a boolean normal form
(e.g. CNF), hence the graph of elemental species and reactions is in fact
a tripartite graph (with partitions always in a cycle: species elemental states,
booleans, elemental reactions). It is not a bipartite graph because of the `OR`
gates (or `AND` gates negated with inhibitory contingencies: `x` or `K+`).


#### Examples ####

The following boolean list:

    <Ste7-5-5-11>,AND
    <STE11-7>,OR

and boolean terms list:

    <Ste7-5-5-11>,Ste5_[MEKK]--Ste11
    <Ste7-5-5-11>,Ste5_[MEK]--Ste7
    <Ste7-5-5-11>,Ste5_[BD/Ste5]--Ste5_[BD/Ste5]
    <STE11-7>,Ste7--Ste11
    <STE11-7>,<Ste7-5-5-11>

reads as:

- Boolean `<Ste7-5-5-11>` requires `Ste5` to be bound to `Ste11` (via `MEKK`
  domain) and to `Ste7` (via `MEK` domain) and to another `Ste5` (via `BD:Ste5`
  domain),
- Boolean `<STE11-7>` requires `Ste7` to be bound to `Ste11` (directly) or
  bound as described by the boolean `<Ste7-5-5-11>`, i.e. via the `Ste5` dimer.



### Contingencies ###

Each contingency is in the following format:

    ${Contingency ID},
    Target,
    ${Contingency type},
    Effector

where contingency type denotes either an activatory effect (`!`, `K+`),
an inhibitory effect (`x`, `K-`), known no effect (`0`), or an unknown effect
(`?`; same as no contingency).

Target is one of the following:

1. system output - written in square bracketes, e.g. `[PRE-transcription]`;
   currently defined in place, i.e. implicitly via contingencies;
2. ${Reaction ID} - defined in the reactions list;
3. ${Boolean ID} - defined in the booleans list.

Effector is one of the following:

1. system input - written in square bracketes, e.g. `[Turgor]`, defined via
   contingencies;
2. species elemental state - defined implicitly as a product in the
   accompanying list of reactions;
3. ${Boolean ID} - defined in the booleans list.

*Remark*: multiple contingencies to a single elemental reaction are joined with
`AND` (cf. CNF for booleans, with `OR` gates only).

*Remark*: input can be also an output; inputs and outputs currently are
implicitly defined `AND` booleans.

*Remark*: if both target and effector are booleans, then they must be different.
(It also means that the booleans are not in a normal form, i.e. the data graph
is not tripartite.)

#### Example ####

The following contingency with species elemental states effector, using reaction
from the reactions example:

    C1,R2,!,Bck1--Mkk1

reads as: reaction of phosphorylation of `Mkk1` by `Bck1` (`R2`) requires that these
species are first bound (`Bck1--Mkk1` elemental state is a product of reaction `R1`).

Analogously, for the following two reactions of phosphorylation of two residues of
`Ste7` by `Ste11` (in the reaction key notation):

    R4,Ste11[KD]_P+_Ste7[AL(S359)]
    R5,Ste11[KD]_P+_Ste7[AL(T363)]

the following two contingencies, using boolean from the booleans example:

    C4,R4,!,<Ste7-5-5-11>	C5,R5,!,<Ste7-5-5-11>

read as: `Ste11` needs to be in the `Ste5` dimer-based scaffold with `Ste7` in order
to activate the latter via phosphorylation.



Contact
-------

    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>
