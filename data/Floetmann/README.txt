Floetmann
======

CSV formats RxnCon data converted from the `Flöttmann_et_al-TableS5-140905.xls`
XLS format data.

The adjustments included:
* genetic evidence separator change (`g_` to `g:`)
* removal of the underscore preceeding domain info (i.e. `_[`  to `[`)
* `<PRE-transcription>` to `[PRE-transcription]`
* IDs for reactions being targets in contingencies
* all inputs in square brackets (e.g. `MF{a,alpha}` to `[MF{a,alpha}]`
  * dealing w/`MFalpha` being input and an reactant (`r4` and `r100`) by adding
    the following contingency to close the negative feedback loop:

        c319,r4,!,MFalpha[(L6-K7)]

