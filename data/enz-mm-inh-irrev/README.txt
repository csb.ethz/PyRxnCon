enz-mm-inh-irrev
================

Irreversible enzymatic reaction with mechanism in the Michaelis-Menten
framework, and with non-competitive inhibition.
