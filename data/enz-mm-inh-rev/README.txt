enz-mm-inh-rev
==============

Reversible enzymatic reaction with mechanism in the Michaelis-Menten framework,
and with non-competitive inhibition.
