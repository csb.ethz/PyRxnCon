HMK-12
======

XLS and CSV formats RxnCon data and a graph PDF created and used by Hans-Michael
Kaltenbach in ca. 2012.

Everything outside of the `misc` folder has been adjusted to the current data
format.
