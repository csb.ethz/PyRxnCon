Intro
=====

This is a Python package for qualitative analysis of chemical reaction networks, using the RxnCon compositional language of elemental species and reactions [[1]].

Quickstart
==========

First get the graph package NetworkX, which you can get via PyPi (pure Python) [[2]]. Now, download the latest release of the PyRxnCon [[3]].

For an example of comparative qualitative models comparison: have a look at the `semova_alpha/README.md`. To run an example of qualitative perturbation analysis see `scripts/README.md`.

Once you get a little familiar with the package, you can start playing with your own models. Brief description of the RxnCon model and data format variant used here is contained in `data/README.md`; examples are in the same folder.

Vision
======

First, the qualitative models validation requires full integration with the `pyrxncon.graphs` in particular (move `semova_alpha` to `pyrxncon.semova`).

Next, the eSR graph has to be updated for the new sound ODE-set semantics for RxnCon; add accompanying graph motifs tests.


Authors
=======

    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@bsse.ethz.com>

Funding
=======

This project was partially funded by Swiss National Science Foundation grant no. 141264.

References
==========

[1]: http://rxncon.org/
1. [RxnCon Web site][1]
[2]: https://pypi.python.org/pypi/networkx
2. [NetworkX at PyPi][2]
[3]: https://gitlab.com/csb.ethz/PyRxnCon/tags
3. [GitLab: PyRxnCon package releases][3]

 
