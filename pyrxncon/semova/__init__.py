# -*- coding: utf-8 -*-
"""Semova module

Semi-automated model validation against the RxnCon data.


Example:
    ...


Authors:
    Janina Linnik <janina.linnik@bsse.ethz.ch>
"""
