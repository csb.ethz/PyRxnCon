# -*- coding: utf-8 -*-
"""
Scripts utilities
"""
import os
import errno
import datetime



#===============================================================================
# print
#===============================================================================
def print_usage(script_name, err_msg=None):
    if err_msg:
        print 'Error: %s\n' % err_msg
    print "For more information run:\n\tpython %s --help\n" % script_name

#===============================================================================
# date & time
#===============================================================================
def now_stamp():
    return datetime.datetime.now().strftime("%y%m%d_%H%M%S")

#===============================================================================
# OS
#===============================================================================
def mkdir_p(path, *args, **kwargs):
    # Src: http://stackoverflow.com/a/600612
    try:
        os.makedirs(path, *args, **kwargs)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def out_fn_ext(dir_path, file_pref):
    def out_fn(file_ext):
        return os.path.join(dir_path, '%s%s%s' % (file_pref,os.extsep,file_ext))
    return out_fn
