"""
Python OO meta utilities
"""
import abc
import inspect


def all_subclasses(cls):
    """
    Get set of all descendant classes of a `cls` class.
    """
    if not inspect.isclass(cls):
        raise ValueError('Expected a class argument.')
    subclasses = set(cls.__subclasses__())
    for subcls in subclasses:
        subclasses = subclasses.union(all_subclasses(subcls))
    return subclasses


def all_concrete_subclasses(cls):
    """
    Get all concrete descendant classes of `cls`, i.e. ones for which
    `is_abstract_class` is false.
    """
    return set(sbcls for sbcls in  all_subclasses(cls) if is_concrete_class(sbcls))
def is_concrete_class(cls):
    """
    Test if `cls` class is not an `abc` module abstract class.
    """
    return not is_abstract_class(cls)
def is_abstract_class(cls):
    """
    Test if `cls` class is abstract (in the `abc` module sense).
    """
    if not inspect.isclass(cls):
        raise ValueError('Expected a class argument.')
    return is_abc_class(cls) and (len(cls.__dict__.get('__abstractmethods__',set())) > 0)
def is_abc_class(cls):
    """
    Test if `cls` class is an `abc` module class.
    """
    if not inspect.isclass(cls):
        raise ValueError('Expected a class argument.')
    return  (cls.__metaclass__ is abc.ABCMeta)
class abstractstatic(staticmethod):
    """
    Abstract static method decorator to correctly work with the `abc` module.

    Src: http://stackoverflow.com/a/4474495
    """
    __slots__ = ()
    def __init__(self, method):
        super(abstractstatic, self).__init__(method)
        method.__isabstractmethod__ = True
    __isabstractmethod__ = True
# class abstractclass(classmethod):
#     """
#     Abstract class method decorator to correctly work with the `abc` module.
#
#     Src: http://stackoverflow.com/a/11218474
#     """
#     # slots ??
#     def __init__(self, method):
#         super(abstractclassmethod, self).__init__(method)
#         method.__isabstractmethod__ = True
#     __isabstractmethod__ = True

