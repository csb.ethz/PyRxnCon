# -*- coding: utf-8 -*-
"""Utils module

Utility functions.


Example:
    ...


Authors:
    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>
"""
