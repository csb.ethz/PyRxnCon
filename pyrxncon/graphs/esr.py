# -*- coding: utf-8 -*-
"""
Base class for the RxnCon elemental species and elemental reactions graph (eSR;
RxnCon regulatory graph).
"""
import networkx as nx
from pyrxncon.data.booleans import create_boolean_object, SignAnd, SignOr



class ESRGraph(object):
    """RxnCon Elemental Species and Reactions graph (eSR; regulatory graph).

    The eSR graph is a directed tripartite graph with sign-labeled edges. The
    eSR graph is tripartite; except for elemental species states nodes and
    elemental reactions nodes, it contains AND or OR boolean nodes. Edges are
    always directed from species to booleans to reactions, back to species.
    The latter are reaction edges, whereas the first two are contingency edges.

    Each edge is labelled with its type (production/consumption for reaction
    edges, stimulation/inhibition for contingency edges), which unambiguously
    defines sign of each edge.

    In the non-normalised form of booleans they may be multi-layered or ommited,
    i.e. contingency may be from boolean to boolean or contingency may be
    specified directly from the species nodes to reaction nodes.

    Attention: sign interpretation differs form the contingencies one with
    `None` denoting undefined (missing) sign and `0` denoting undecided
    (conflicting) sign.

    Attributes:
        G (networkx.DiGraph): the actual directed graph object wrapped within
            the `ESRGraph` class.
    """

    _CONT_SIGN_TO_COLOR = {
        -1: 'magenta',
        1: 'darkgreen',
        0: 'yellow',
        None: ''
    }
    _NODE_SIGN_TO_COLOR = {
        -1: '#FF9999', # lightred
        1: '#99FF99', # lightgreen
        0: '#FFFF99', # lightyellow
        None: ''
    }
    _BOOL_TYPE_TO_STYLE = {
       SignAnd.operator() : '',
       SignOr.operator() : 'dotted'
    }


    def __init__(self, **attr):
        self.G = nx.DiGraph(**attr)

        self.rxn = dict()
        self.spc = set()
        self.bol = dict()

        self.con = dict()


    def _add_node(self, v, obj, **kwargs):
        return self.G.add_node(v, obj=obj, **kwargs)
    def _add_edge(self, u, v, sign, obj, **kwargs):
        # TODO: move sign to color coding here (attention: different for diff types of edges)
        return self.G.add_edge(u, v, sign=sign, obj=obj, **kwargs)


    def node_obj(self, v):
        """
        Node reference to the corresponding RxnCon data object (`None` for
        elemental species).
        """
        return self.G.node[v]['obj'] # always there (KeyError otherwise)
    def edge_obj(self, u, v):
        """
        Edge ref to the corresponding RxnCon data object (not `None` only for
        contingency edges).
        """
        return self.G.edge[u][v]['obj'] # always there (KeyError otherwise)

    def edge_sign(self, u, v):
        """
        Edge sign - always set.
        """
        return self.G.edge[u][v]['sign'] # always there (KeyError otherwise)
    def node_sign(self, v):
        """
        Node sign can be set, but by default it's not (`None`).
        """
        return self.G.node[v].get('sign',None)
    def mark_node(self, v, sgn):
        """
        Set node sign. Returns `True` if node was marked and its value has
        changed, `False` otherwise.
        """
        if sgn not in [-1,1,0]:
            raise ValueError('Sign value %s is invalid.' % sgn)
        return self._mark_node(v, sgn)
    def unmark_node(self, v):
        """
        Remove node sign. Returns `True` if node was marked, `False` otherwise.
        """
        return self._mark_node(v, None)
    def _mark_node(self, v, sgn):
        mark = (sgn != self.node_sign(v))
        if mark:
            d = self.G.node[v]
            d['sign'] = sgn
            d['color'] = self._NODE_SIGN_TO_COLOR[sgn]
        return marked
    def is_marked(self):
        for v in self.G.nodes_iter():
            if self.node_sign(v) is None:
                return False
        return True


    def input_nodes_iter(self):
        for bid, bobj in self.bol.iteritems():
            if bobj.io and self.G.out_degree(bid) > 0:
                yield bid
    def input_nodes(self):
        return list(self.input_nodes_iter())
    def output_nodes_iter(self):
        for bid, bobj in self.bol.iteritems():
            if bobj.io and self.G.in_degree(bid) > 0:
                yield bid
    def output_nodes(self):
        return list(self.output_nodes_iter())


    def add_reactions(self, rxn):
        for r in rxn:
            self.add_reaction(r)
    def add_reaction(self, r):
        self.rxn[repr(r)] = r
        self._add_reaction(r)
    def _add_reaction(self, r):
        r_str = repr(r)
        self._add_node(r_str, r,  **self._reaction_node_kwargs(r))
        self.add_substrate(r.substrate(), r_str)
        self.add_product(r.product(), r_str)
    @staticmethod
    def _reaction_node_kwargs(r):
        return {
            'style':'',
            'shape':'box',
            'label':str(r)
        }

    def add_substrate(self, subs, reac):
        return self.add_species(subs, reac, -1, **self._substrate_edge_kwargs(subs, reac))
    def add_product(self, prod, reac):
        return self.add_species(prod, reac, 1, **self._product_edge_kwargs(prod, reac))
    @staticmethod
    def _substrate_edge_kwargs(subs,reac):
        return {'color':'red', 'label':'consume', 'arrowhead':'tee'}
    @staticmethod
    def _product_edge_kwargs(prod,reac):
        return {'color': 'green', 'label':'produce', 'arrowhead':'vee'}
    def add_species(self, spec, reac, sign, **edge_kwargs):
        if spec:
            self.spc.add(spec)
            self._add_species(spec, reac, sign, **edge_kwargs)
            return True
        return False
    def _add_species(self, spec, reac, sign, **edge_kwargs):
        self._add_node(spec, None, **self._species_node_kwargs(spec))
        self._add_edge(reac, spec, sign, None, **edge_kwargs)
    @staticmethod
    def _species_node_kwargs(spec):
        return {'style':'filled', 'shape':''}

    def add_booleans(self, bol, **kwargs):
        for b in bol:
            self.add_boolean(b)
    def add_boolean(self, b, check=True):
        self.bol[repr(b)] = b
        self._add_boolean(b, check)
    def _add_boolean(self, b, check):
        b_str = repr(b) # '%s: %s' % (repr(b), b.operator())
        self._add_node(b_str, b, **self._boolean_node_kwargs(b))
        ekwargs = self._boolean_edge_kwargs(b)
        for bt in b.terms:
            self.add_effector(bt, check)
            self._add_edge(bt, b_str, 1, None, **ekwargs)
    @staticmethod
    def _boolean_node_kwargs(b):
        return {
            'style':'filled',
            'shape': 'invhouse', # ('house' if b.operator() == SignAnd.operator() else 'invhouse')
            'color': ('black' if b.io else '#666666'),
            'fontcolor': 'white'
        }
    @classmethod
    def _boolean_edge_kwargs(cls,b):
        return {
            'style': cls._BOOL_TYPE_TO_STYLE[b.operator()],
            'label': b.operator()
        }


    def can_be_effector(self, effector):
        return effector in self.spc or effector in self.bol
    def add_effector(self,effector,check):
        if not self.can_be_effector(effector):
            b = create_boolean_object(SignAnd.operator(),effector)
            if check and not b.io: # expected in node
                raise MissingEffectorNodeException(effector)
            self.add_boolean(b, check=check)
            return b
        return None
    def can_be_target(self, target):
        return target in self.rxn or target in self.bol
    def add_target(self,target,check,*terms):
        if not self.can_be_target(target):
            b = create_boolean_object(SignAnd.operator(),target)
            if check and not b.io: # expected out node
                raise MissingTargetNodeException(target)
            for t in terms:
                b.add(t)
            self.add_boolean(b, check=check)
            return b
        return None


    def add_contingencies(self, ctg, **kwargs):
        for c in ctg:
            self.add_contingency(c, **kwargs)
    def add_contingency(self, c, check=True):
        self.con[repr(c)] = c
        self._add_contingency(c, check)
    def _add_contingency(self, c, check):
        self.add_effector(c.effector, check)
        self.add_target(c.target, check, c.effector)
        # sanity check
        assert check and self.can_be_target(c.target) and self.can_be_effector(c.effector)
        # get full labels instead of IDs
        effector = self.bol.get(c.effector, None)
        effector = repr(effector) if effector else c.effector
        target = self.bol.get(c.target, self.rxn.get(c.target, None))
        target = repr(target) if target else c.target
        # finally, add the contingency edge
        self._add_edge(effector, target, c.sign(), c, **self._contingency_edge_kwargs(c))
    @classmethod
    def _contingency_edge_kwargs(cls,c):
        return {
            'color':cls._CONT_SIGN_TO_COLOR[c.sign()],
            'label': ('%s: %s' % (c.id, c.type())),
            'arrowhead': ('tee' if c.sign() < 0 else 'vee')
        }





    def remove_nodes(self, nodes):
        return list(self.remove_nodes_iter(nodes))
    def remove_nodes_iter(self, nodes):
        for v in nodes:
            yield self.remove_node(v)
    def remove_node(self, v):
        obj = self.node_obj(v)
        if obj is None:
            self.spc.remove(v)
        else:
            objrep = repr(obj)
            self.rxn.pop(objrep,None)
            self.bol.pop(objrep,None)
        self.G.remove_node(v)
        return obj or v

    def remove_small_components(self, **kwargs):
        return list(self.remove_small_components_iter(**kwargs))
    def remove_small_components_iter(self, n_nodes_min=3):
        for sG in nx.connected_component_subgraphs(self.G.to_undirected()):
            nodes = sG.nodes()
            if len(nodes) < n_nodes_min: # TODO: consider threshold by node types (cf. small examples)
                yield self.remove_nodes(nodes)






    def normal_form(self):
        # TODO CNF for booleans
        raise NotImplementedError




class MissingTargetNodeException(Exception):
    pass
class MissingEffectorNodeException(Exception):
    pass
