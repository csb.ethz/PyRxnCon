# -*- coding: utf-8 -*-
"""
NetworkX `readwrite` and `gpickle` modules proxy.
"""
__all__ = [
   'read_esr_pickle',
   'write_esr_pickle', 'write_esr_dot', 'write_esr_json', 'write_esr', 'ESR_FN_PREFIX'
]

import os
try:
    import cPickle as pickle
except ImportError:
    import pickle

import networkx as nx
from networkx.readwrite import json_graph
from networkx.utils.decorators import open_file




@open_file(0,mode='rb')
def read_esr_pickle(path):
    up = pickle.Unpickler(path)
    esr = up.load()
    esr.G = up.load()
    return esr



@open_file(1,mode='wb')
def write_esr_pickle(esr, path):
    p = pickle.Pickler(path, pickle.HIGHEST_PROTOCOL)
    p.dump(esr)
    p.dump(esr.G)
    # nx.write_gpickle(esr.G, path)

def write_esr_dot(esr, path):
    nx.draw_graphviz(esr.G)
    nx.write_dot(esr.G, path)

@open_file(1,mode='w+')
def write_esr_json(esr, path):
    data = json_graph.node_link_data(esr.G)
    print >> path, data

ESR_FN_PREFIX = 'eSR'
def write_esr(esr, out_fn_ext_fun=(lambda ext: ('%s%s%s' %(ESR_FN_PREFIX,os.extsep,ext))), dot=True, json=False):
    write_esr_pickle(esr, out_fn_ext_fun('pickle'))
    if dot:
        write_esr_dot(esr, out_fn_ext_fun('dot'))
    if json:
        write_esr_json(esr, out_fn_ext_fun('json'))
