# -*- coding: utf-8 -*-
"""Spinass module

Spin assignment (graph marking) of the RxnCon data graphs.


Example:
    ...


Authors:
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>
"""
from esr import mark as mark_esr
