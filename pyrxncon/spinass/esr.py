# -*- coding: utf-8 -*-
"""
`pyrxncon.graphs.ESRGraph` marking (spin assignment)
"""
__all__ = ['mark']

from pyrxncon.data.booleans import SignBool

def diff(a, b):
    b = set(b)
    return [e for e in a if e not in b]

# like nx.bfs_edges but spanning over edges, not nodes, hence w/ edges to visited nodes
def bfs_edges(g, s):
    visited, queue = [], [e for e in g.out_edges_iter(s)]
    while queue:
        e = queue.pop(0)
        if e not in visited:
            visited.append(e)
            queue.extend(diff(g.out_edges_iter(e[1]),visited))
    return visited
# for comparison, w/o edges to visited nodes
# def bfs_edges(g, s):
# 	return nx.bfs_edges(g, s)


def mark_node(esr, v, sgn):
    # esr.mark_node but with a special treatment for booleans
    v_obj = esr.node_obj(v)
    # TODO: factor that out to the booleans directly, i.e. let them keep signs
    #       for terms (and update on marking)
    s = v_obj.apply(
        [esr.node_sign(v_in) for (v_in, _) in esr.G.in_edges_iter(v)] +[sgn]
    ) if isinstance(v_obj, SignBool) else sgn
    # Rem: currently `+[sgn]` above just duplicates sign from one of the input
    # nodes; doesn't hurt and this way one can also e.g. test for possible
    # node marking.
    return False if s is None else esr.mark_node(v, s)

def mark_out_node(esr, n_in, n_out):

    n_in_v = esr.node_sign(n_in)
    if n_in_v is None: # e.g. bool gate
        #print n_in
        return False
    e_v = esr.edge_sign(n_in, n_out)
    if e_v is None: # e.g. '0' (no-influence) contingency
        return False

    n_out_v = esr.node_sign(n_out)
    #print ((n_in, n_in_v), e_v, (n_out, n_out_v))
    n_out_v = 0 if n_out_v is not None and n_out_v != n_in_v * e_v else n_in_v * e_v
    return mark_node(esr, n_out, n_out_v)

def mark(esr, start_nodes_dict):
    """
    A 'forward', partrial (cf. boolean gates) marking.
    """
    # OPT: implement multi-source BFS (cf. http://www-db.in.tum.de/~kaufmann/papers/msbfs.pdf)
    for n,v in start_nodes_dict.iteritems():
        esr.mark_node(n, v) # forced marking, w/o any boolean sheneningans
        # Run BFS until no change to propagate 0s, and
        edges = bfs_edges(esr.G,n)
        n_iter = 0
        while n_iter < 3:#True:
            n_changed_nodes = 0
            for e in edges:
                if mark_out_node(esr, *e):
                    n_changed_nodes +=1
            n_iter += 1
            #print '[#%2d] changes=%d' % (n_iter, n_changed_nodes)
            if n_changed_nodes == 0 and esr.is_marked():
                break
    return esr
