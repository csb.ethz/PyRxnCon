# -*- coding: utf-8 -*-
"""PyRxnCon

Python utilities for the Reactions and Contingencies (`RxnCon`_) format.


References:
.. _RxnCon:
    http://rxncon.org/


Authors:
    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>
"""
