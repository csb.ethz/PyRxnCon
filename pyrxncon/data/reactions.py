# -*- coding: utf-8 -*-
'''
RxnCon reactions objects.
'''
import abc
from pyrxncon.utils import meta

class RxnConKey(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self._key = None

    def __repr__(self, *args, **kwargs):
        return self.key_str()

    def key_str(self):
        if self._key is None:
            self._key = self._key_str()
        return self._key

    @abc.abstractmethod
    def _key_str(self):
        return


class SpeciesKey(RxnConKey):

    def __init__(self, spec, dom, subdom, res):
        super(SpeciesKey, self).__init__()
        self.spec = spec
        self.dom = dom
        self.subdom = subdom
        self.res = res

    def _key_str(self):
    # Remark: if a substrate (source) or a product state of a reaction does not
    #         contain domain, or subdomain, or residue info (e.g. substrate in
    #         the degradation reaction), then respective parts of the species
    #         key string are automatically omitted.
        res = '(%s)' % self.res if self.res else ''
        subdom = '/%s' % self.subdom if self.subdom else ''
        dom_subdom_res = '%s%s%s' % (self.dom,subdom,res)
        dom_subdom_res = '[%s]' % dom_subdom_res if dom_subdom_res else ''
        return '%s%s' % (self.spec, dom_subdom_res)

ER_SUBTYPE_SEP = ':'
class ElementalReaction(RxnConKey):
    __metaclass__ = abc.ABCMeta

    def __init__(self, subtype, rid, specA, domA, subdomA, resA, specB, domB, subdomB, resB):
        super(ElementalReaction, self).__init__()
        self.pref = subtype # hard coded subtype as it doesn't influence reaction behaviour
        self.id = rid
        self.specA = SpeciesKey(specA, domA, subdomA, resA)
        self.specB = SpeciesKey(specB, domB, subdomB, resB)

    # override
    def __repr__(self, *args, **kwargs):
        return self.id
    def __str__(self, *args, **kwargs):
        return '%s: %s' % (repr(self),super(ElementalReaction, self).__repr__())

    def _key_str(self):
        return '%s_%s_%s' % (self.specA.key_str(),self.type(),self.specB.key_str())
    def type(self):
        return '%s%s' % (self.subtype(),self.base_type())
    def subtype(self):
        return ('%s%s' % (self.pref,ER_SUBTYPE_SEP)) if self.pref else ''
    def has_only_genetic_evidence(self):
        return self.pref == 'g'
    @meta.abstractstatic
    def base_type():
        return

    @abc.abstractmethod
    def substrate(self):
        return
    @abc.abstractmethod
    def product(self):
        return

####
# Interactions
#
class InteractionReaction(ElementalReaction):
    # partial implementation
    def substrate(self):
        return ''
    def product(self):
        return '%s--%s' % (self.specA.key_str(), self.specB.key_str())
class PpiReaction(InteractionReaction):
    @staticmethod
    def base_type():
        return 'ppi'
class IntraProteinInteraction(InteractionReaction):
    @staticmethod
    def base_type():
        return 'ipi'
class NonProteinInteraction(InteractionReaction):
    @staticmethod
    def base_type():
        return 'i'
class DnaBinding(InteractionReaction):
    @staticmethod
    def base_type():
        return 'BIND'

####
# Synthesis/degradation
#
class DegradationReaction(ElementalReaction):
    @staticmethod
    def base_type():
        return 'DEG'
    def substrate(self):
        return self.specB.key_str()
    def product(self):
        return ''


####
# Covalent modifications
#
class Modification(object):
    __metaclass__ = abc.ABCMeta
    @meta.abstractstatic
    def modification():
        return
    def modified_str(self, spec):
        return '%s-{%s}' % (spec.key_str(), self.modification())
class PhosphoGroupModification(Modification):
    @staticmethod
    def modification():
        return 'P'

class CovalentAdditionReaction(ElementalReaction,Modification):
    # partial implementation
    def substrate(self):
        return ''
    def product(self):
        return self.modified_str(self.specB)
class Phosphorylation(CovalentAdditionReaction,PhosphoGroupModification):
    @staticmethod
    def base_type():
        return 'P+'
class AutoPhosphorylation(CovalentAdditionReaction,PhosphoGroupModification):
    @staticmethod
    def base_type():
        return 'AP'
class GdpExchangeReaction(CovalentAdditionReaction,PhosphoGroupModification):
    @staticmethod
    def base_type():
        return 'GEF'
class Ubiqutination(CovalentAdditionReaction):
    @staticmethod
    def base_type():
        return 'Ub+'
    @staticmethod
    def modification():
        return 'Ub'
class ProteolyticCleavage(CovalentAdditionReaction):
    # doesn't make sense as a subclass of covalent addition but it's implemented
    # this way in the data
    @staticmethod
    def base_type():
        return 'CUT'
    @staticmethod
    def modification():
        return 'Truncated'


class CovalentRemovalReaction(ElementalReaction,Modification):
    # partial implementation
    def substrate(self):
        return self.modified_str(self.specB)
    def product(self):
        return ''
class Dephosphorylation(CovalentRemovalReaction,PhosphoGroupModification):
    @staticmethod
    def base_type():
        return 'P-'
class GtpHydrolysisReaction(CovalentRemovalReaction,PhosphoGroupModification):
    @staticmethod
    def base_type():
        return 'GAP'

class CovalentTransferReaction(ElementalReaction,Modification):
    # partial implementation
    def substrate(self):
        return self.modified_str(self.specA)
    def product(self):
        return self.modified_str(self.specB)
class PhosphoTransferReaction(CovalentTransferReaction,PhosphoGroupModification):
    @staticmethod
    def base_type():
        return 'PT'



class UnsupportedElementalReactionType(Exception):
    pass

_ER_CLASSES = meta.all_concrete_subclasses(ElementalReaction)
_DATA_ERSUBTYPE_SEP = ER_SUBTYPE_SEP #'_'
def create_elemental_reaction_object(rtype, *rargs):
    """
    ElementalReaction objects factory function
    """
    base_type = 'not read'
    for er_cls in _ER_CLASSES:
        subtype,base_type = rtype.split(_DATA_ERSUBTYPE_SEP) if _DATA_ERSUBTYPE_SEP in rtype else ('',rtype)
        if er_cls.base_type() == base_type:
            return er_cls(subtype, *rargs)
    raise UnsupportedElementalReactionType("%s" % base_type)
