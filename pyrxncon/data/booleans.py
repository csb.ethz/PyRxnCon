# -*- coding: utf-8 -*-
'''
Contingencies objects
'''
import abc
import re
from pyrxncon.utils import meta

# def _not_none_iter(collection):
#     for element in collection:
#         if element is not None:
#             yield element

class SignBool(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, bid):
        self.id = bid
        self.io = self.is_io_repr(bid)
        self.terms = set()

    @staticmethod
    def is_io_repr(brepr):
        return bool(re.search('^\[.+\]$', brepr))

#     @staticmethod
#     def strip_id(bid):
#         return re.sub('^[\[<](.+)[>\]]$','\g<1>',bid)

    def __repr__(self):
        return self.id #('[%s]' if self.io else '<%s>') % self.id

    def __str__(self, *args, **kwargs):
        return '%s: %s' % (repr(self), (' %s ' % self.operator()).join(self.terms))

    @meta.abstractstatic
    def operator():
        return

    def add(self, term):
        self.terms.add(term)



    @classmethod
    def apply(cls, sgns):
        """
        Process sign inputs according to the boolean operator.

        `None` and `0` are used to distinguish between logic gate  undefined
        because of:
          1. a lack of information;
          2. a sign conflict.
        """
        # All gate relevant signs should have the same sign
        # list() in case of generators
        return cls.same_sign(cls.relevant_signs(sgns))

    @classmethod
    def same_sign(cls, sgns):
        if len(sgns) == 0:
            return None
        if all(s < 0 for s in sgns):
            return -1
        if all(s > 0 for s in sgns):
            return 1
        return 0

    @meta.abstractstatic
    def relevant_signs(sgns):
        # TODO this depends on application: in case of up/down marking there
        #      is no diff between And/Or; in case of shut-down marking there is
        #      a diff between And/Or
        #      put that semantics into marking script (or ev. make it possible
        #      to choose approriate behaviour while marking)
        # Rem: return collection, not a generator for `len` test in `same_sign`
        return





class SignAnd(SignBool):
    @staticmethod
    def operator():
        return 'AND'
    @staticmethod
    def relevant_signs(sgns):
#         # all signs if defined, otherwise None
#         if not all(s is not None for s in sgns):
#             # rem: emtpy `sgnl` is returned below
#             return []
#         return list(sgns)
        # all defined signs - no diff to Or gate (cf. discussion w/ MK)
        return [s for s in sgns if s is not None]

class SignOr(SignBool):
    @staticmethod
    def operator():
        return 'OR'
    @staticmethod
    def relevant_signs(sgns):
        # all defined signs
        return [s for s in sgns if s is not None]

class UnsupportedBoolType(Exception):
    pass

_BOOL_CLASSES = meta.all_concrete_subclasses(SignBool)
def create_boolean_object(btype, *bargs):
    """
    Contingency objects factory function
    """
    for bool_cls in _BOOL_CLASSES:
        if bool_cls.operator() == btype:
            return bool_cls(*bargs)
    raise UnsupportedBoolType("%s" % btype)

# Below: a bit of an overkill for now, but may help avoid the `None`, `0` signs
# interpretation mistakes.
#
# class Sign(object):
#     __metaclass__ = abc.ABCMeta
#
#     @meta.abstractstatic
#     def value():
#         return
#     @staticmethod
#     def is_defined():
#         return True
#     @staticmethod
#     def is_decided():
#         return True
# class Plus(Sign):
#     @staticmethod
#     def value():
#         return 1
# class Minus(Sign):
#     @staticmethod
#     def value():
#         return -1
# class Undefined(Sign):
#     @staticmethod
#     def value():
#         return None
#     @staticmethod
#     def is_defined():
#         return False
# class Undecided(Sign)
#     @staticmethod
#     def value():
#         return 0
#     @staticmethod
#     def is_decided():
#         return False
#
# class UnsupportedSignType(Exception):
#     pass
#
# _SIGN_CLASSES = meta.all_concrete_subclasses(Sign)
# def create_sign_object(sgn, *sargs):
#     """
#     Signs objects factory function
#     """
#     try:
#         sgnv = int(sgn)
#         for sign_cls in _SIGN_CLASSES:
#             if sign_cls.value() == sgnv:
#                 return sign_cls(*sargs)
#     except ValueError:
#         pass
#     raise UnsupportedSignType("%s" % btype)
