# -*- coding: utf-8 -*-
'''
Contingencies objects
'''
import abc
from pyrxncon.utils import meta



class Contingency(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, cid, target, effector):
        self.id = cid
        self.effector = effector
        self.target = target

    def __repr__(self, *args, **kwargs):
        return self.id

    def __str__(self, *args, **kwargs):
        return '%s: %s %s %s' % (repr(self),self.target,self.type(),self.effector)

    @meta.abstractstatic
    def type():
        return
    @meta.abstractstatic
    def sign():
        return


####
# Activation
class ActivationContingency(Contingency):
    # partial implementation
    @staticmethod
    def sign():
        return 1
class RequiringContingency(ActivationContingency):
    @staticmethod
    def type():
        return "!"
class StimulatingContingency(ActivationContingency):
    @staticmethod
    def type():
        return "K+"

####
# Deactivation
class DeactivationContingency(Contingency):
    # partial implementation
    @staticmethod
    def sign():
        return -1
class ExcludingContingency(DeactivationContingency):
    @staticmethod
    def type():
        return "x"
class InhibitingContingency(DeactivationContingency):
    @staticmethod
    def type():
        return "K-"

####
# Neutral
class NoInfluenceContigency(Contingency):
    @staticmethod
    def sign():
        return None
    @staticmethod
    def type():
        return "0"

####
# Unknown
class UnknownContigency(Contingency):
    @staticmethod
    def sign():
        return 0
    @staticmethod
    def type():
        return "?"


class UnsupportedContingencyType(Exception):
    pass

_CON_CLASSES = meta.all_concrete_subclasses(Contingency)
def create_contingency_object(ctype, *cargs):
    """
    Contingency objects factory function
    """
    for con_cls in _CON_CLASSES:
        if con_cls.type() == ctype:
            return con_cls(*cargs)
    raise UnsupportedContingencyType("%s" % ctype)
