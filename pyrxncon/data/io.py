# -*- coding: utf-8 -*-
'''
RxnCon I/O utilities
'''
import csv

from contingencies import create_contingency_object
from reactions import create_elemental_reaction_object
from booleans import create_boolean_object



#===============================================================================
# Auxiliary
#===============================================================================
def _process_csv_lines(csv_path, process_line_fun, delimiter=',', quotechar='"', **extra_csv_reader_kwargs):
    """
    Read CSV text file from `csv_path` and process line by line using the
    supplied `process_line_fun` function.

    Keyword arguments for the
    """
    with open(csv_path, 'rU') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=delimiter,
                               quotechar=quotechar, **extra_csv_reader_kwargs)
        return [process_line_fun([cv.strip() for cv in line]) for line in csvreader if line]

def line_reader_by_type(type_idx,args_nr):
    """
    Decorator for functions for reading RxnCon data into objects by object type
    field (e.g. contingency "!" or reaction "ppi" or boolean "AND").

    Args::
        `type_idx` (int): object type field index
        `args_nr` (int): total number of fields (and function args); additional
            annotations in the data are allowed and will be dropped.
    """
    def read_line_by_type_direct_decorator(read_line_fun):
        def read_line_by_type(line):
            otype = line[type_idx]
            args = line[:type_idx] + line[(type_idx+1):args_nr]
            return read_line_fun(otype, *args)
        return read_line_by_type
    return read_line_by_type_direct_decorator

#===============================================================================
# Main
#===============================================================================
def read_contingencies_csv(path):
    """
    Read RxnCon contingencies list from the CSV file given `path`.
    """
    return _process_csv_lines(path, _read_contingency)
_read_contingency = line_reader_by_type(2,4)(create_contingency_object)

def read_reactions_csv(path):
    """
    Read RxnCon reactions list from the CSV file given `path`.
    """
    return _process_csv_lines(path, _read_reaction)
_read_reaction = line_reader_by_type(5,10)(create_elemental_reaction_object)

def read_booleans_csvs(path_def,path_terms,check=False):
    """
    Read RxnCon booleans list from the definitions in the `path_def` CSV file
    and terms in the `path_terms` CSV file.
    """
    bools = _process_csv_lines(path_def, _read_boolean) if path_def else []
    if path_terms:
        _process_csv_lines(path_terms, _read_term(bools,check))
    return bools
_read_boolean = line_reader_by_type(1,1)(create_boolean_object)
_DATA_BID_IDX = 0
_DATA_BTERM_IDX = 1
def _read_term(bools,check):
    bools_dict = dict([(b.id,b) for b in bools])
    def read_term(line):
        # TODO TU find bool and add term (change: __repr__ -> __str__ + __repr__ == self.id for simple list operations?)
        bid = line[_DATA_BID_IDX]
        if bools_dict.has_key(bid):
            bools_dict[bid].add(line[_DATA_BTERM_IDX])
        elif check:
            raise ValueError('Undefined boolean "%s".' % bid)
        return None
    return read_term
