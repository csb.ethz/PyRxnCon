#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    cont_rkey_to_rid - convert RxnCon contingencies targets from the reaction key
        string to reaction ID format


Usage:
    python cont_rkey_to_rid.py reactions_csv contingencies_csv


Attributes:

    reactions_csv:
        Path to a CSV file with RxnCon reactions list.

    contingencies_csv:
        Path to a CSV file with RxnCon contingencies list


Options:
    -h, --help:
        Print this info.


Output:
    Prints out converted contingencies CSV.


Examples:
    ...

Authors:
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>


Date: 2015 May 27
"""
import getopt
import sys

from pyrxncon.utils.scripts import print_usage
from pyrxncon.data import read_contingencies_csv, read_reactions_csv
from pyrxncon.data.reactions import ER_SUBTYPE_SEP



def usage(*args,**kwargs):
    return print_usage('cont_rkey_to_rid.py',*args,**kwargs)

def print_help():
    print __doc__



if __name__ == '__main__':
#===============================================================================
# parse args
#===============================================================================
    options, argv = getopt.getopt(sys.argv[1:], 'h', ['help'])

    # parse options
    for opt, arg in options:
        if opt in ('-h', '--help'):
            print_help()
            sys.exit(0)

    # parse args
    nargs = len(argv)
    # required args
    if  nargs < 2:
        usage('Missing paths to reactions and contingencies CSV files.')
        sys.exit(1)
    path_rxn,path_con = argv[0:2]
#===============================================================================
# import CSV data
#===============================================================================
    rxn = read_reactions_csv(path_rxn)
    #print [str(r) for r in rxn]
    con = read_contingencies_csv(path_con)
    #print [str(c) for c in con]
#===============================================================================
# convert contingencies to reactions ids and print them out
#===============================================================================
    for c in con:
        rid = c.target
        for r in rxn:
            key_strs = [r._key_str()]
            r.pref = '' if len(r.pref) > 0 else 'g'
            key_strs.append(r._key_str())
            if rid in key_strs:
                rid = r.id
                break
        print "%s,%s,%s,%s" % (c.id,rid,c.type(),c.effector)

    sys.exit(0)
