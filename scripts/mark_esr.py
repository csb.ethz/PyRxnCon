#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    mark_esr - sign mark RxnCon elemental species and reactions graph (in BFS order).


Usage:
    python mark_esr.py pickle_path [start_node1 ... start_nodeN]


Attributes:
    path: path to RxnCon regulatory graph `pickle`_ file.

    start_node1 ... start_nodeN: source nodes for marking


Options:
    -h, --help: print this info


Output:
    ...


Examples:
    ...


.. _pickle:
    https://docs.python.org/2/library/pickle.html


Authors:
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>

Date: 2015 Mar 17
"""
import getopt
import os
import sys

from pyrxncon.utils.scripts import print_usage, out_fn_ext
from pyrxncon.graphs import read_esr_pickle, write_esr
from pyrxncon.spinass import mark_esr


#===============================================================================
# auxiliary functions
#===============================================================================
def usage(*args,**kwargs):
    return print_usage('mark_esr.py',*args,**kwargs)

def print_help():
    print __doc__



def parse_start_nodes(esr, start_nodes):
    if not start_nodes: # pick a first input node, or if none, then globally first one
        try:
            start_node = esr.input_nodes_iter().next()
        except IndexError:
            start_node = esr.G.nodes()[0]
        start_nodes_dict = {start_node:1}
    else:
        start_nodes_dict = {}
        for n in start_nodes:
            nv = n.split("=")
            if nv[0] not in esr.G.nodes():
                usage('No "%s" node to start with.' % nv[0])
                sys.exit(1)
            start_nodes_dict[nv[0]] = int(nv[1]) if len(nv) > 1 else 1
            assert start_nodes_dict[nv[0]] in [-1, 0, 1]
    return start_nodes_dict


if __name__ == '__main__':
#===============================================================================
# input args
#===============================================================================
    options, argv = getopt.getopt(sys.argv[1:], 'h', ['help'])

    # parse options
    for opt, arg in options:
        if opt in ('-h', '--help'):
            print_help()
            sys.exit(0)

    # parse args
    nargs = len(argv)
    # required args
    if  nargs < 1:
        usage('Path to eSR graph gpickle file is required.')
        sys.exit(1)
    path = argv[0]
    # optional args
    start_nodes = None
    if nargs > 1:
        start_nodes = argv[1:]

#===============================================================================
# initialize
#===============================================================================
    esr = read_esr_pickle(path)

    # initial sanity check
    if esr.G.size() == 0:
        print 'Empty graph is already marked.'
        sys.exit(0)

    start_nodes_dict = parse_start_nodes(esr, start_nodes)
#===============================================================================
# mark
#===============================================================================
    esr = mark_esr(esr, start_nodes_dict)

#===============================================================================
# export
#===============================================================================
    [out_dir, out_fn_str] = os.path.split(path)
    out_fn_pref = os.path.splitext(out_fn_str)[0]
    out_fn_pref = '%s-marked-%s' % (out_fn_pref, '__'.join('%s_%s' % (n,v) for n,v in start_nodes_dict.iteritems()))
    try:
        out_fn = out_fn_ext(out_dir,out_fn_pref)
        write_esr(esr, out_fn)
    except:
        exc_info = sys.exc_info()
        print "Unexpected error:", exc_info[1]
        #import traceback; traceback.print_tb(exc_info[2])
        sys.exit(1)

#===============================================================================
# report
#===============================================================================
    print 'Good news, the RxnCon eSR graph has been marked and exported.'

    # report consistency
    marked = [n for n in esr.G.nodes_iter() if esr.node_sign(n) is not None]
    undecided = [n for n in marked if esr.node_sign(n) == 0]
    print "Marked %d out of %d nodes, staring with: %s." % (len(marked), esr.G.size(), ', '.join(start_nodes))
    print "Inconsistent nodes (%d):" % len(undecided)
    print '\n\t'.join(undecided)

    img_ext = 'pdf'
    print 'To plot you can run:'
    print '\tdot -x -Goverlap=scale -T%s "%s" > "%s"' % (img_ext, out_fn('dot'), out_fn(img_ext))

    sys.exit(0)
