PyRxnCon Scripts
================

Python scripts for running PyRxnCon common tasks such as data and model graphs
import and export, comparison and marking.


Installation
------------

Install `networkx` package and make sure that the `pyrxncon` package is in the
Python search path (cf. `PYTHONPATH` environmental variable).


Examples
--------


### Create elemental species and reactions graph from data

E.g. from the parent directory, run:

    PYTHONPATH=. scripts/create_esr.py -o out/ data/HMK-12/YM4-{reactions,contingencies-w_bool,booleans,booleans-terms}.csv

Visualise according to the instruction printed on the screen, e.g.:
    
    dot -x -Goverlap=scale -Tpdf out/eSR_Graph.dot > out/eSR_Graph.pdf


### Compare data and model graphs

...


### Forward mark graph vertices according to a perturbation

To start e.g. at the `[MFa]` and `[MFalpha]` inputs downregulated, e.g. from the parent directory, run:
    
    PYTHONPATH=. scripts/mark_esr.py out/eSR_Graph.pickle [Mfa]=-1 [MFalpha]=-1



Contact
-------

    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>
