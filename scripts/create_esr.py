#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    create_esr - import the RxnCon data into the eSR graph format


Usage:
    python create_esr.py reactions_csv contingencies_csv [booleans_csv booleans-terms_csv]


Attributes:

    reactions_csv:
        Path to a CSV file with RxnCon reactions list.

    contingencies_csv:
        Path to a CSV file with RxnCon contingencies list

    booleans_csv:
        Path to a CSV file with RxnCon booleans list

    booleans-terms_csv:
        Path to a CSV file with RxnCon booleans terms list


Options:
    -h, --help:
        Print this info.

    -o PATH, --output-dir=PATH:
        Output directory path, by default " o_rxncon_{timepstamp}" in the
        current directory. If needed script creates directory and all parent
        directories.


Output:
    Creates output directory with the RxnCon regulatory graph files in
    `gpickle`_,	`JSON node-link like`_, and DOT formats.

    To visualise the graph in the PDF format using `Graphviz`_ run:
        dot -x -Goverlap=scale -Tpdf eSR.dot > eSR.pdf


Examples:
    ...


References:
.. _gpickle:
    http://networkx.lanl.gov/reference/readwrite.gpickle.html
.. _JSON node-link like:
    http://networkx.github.io/documentation/latest/reference/readwrite.json_graph.html
.. _Graphviz:
    http://www.graphviz.org/


Authors:
    Janina Linnik <janina.linnik@bsse.ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@gmail.com>


Date: 2015 Apr 28
"""
import getopt
import sys

from pyrxncon.utils.scripts import print_usage, now_stamp, mkdir_p, out_fn_ext
from pyrxncon.data import read_contingencies_csv, read_reactions_csv, read_booleans_csvs
from pyrxncon.graphs import ESRGraph, write_esr, ESR_FN_PREFIX


# print error message or usage
def usage(*args,**kwargs):
    return print_usage('create_esr.py',*args,**kwargs)

def print_help():
    print __doc__



if __name__ == '__main__':
#===============================================================================
# parse args
#===============================================================================
    options, argv = getopt.getopt(sys.argv[1:], 'ho:', ['help','output-dir'])

    # parse options
    out_dir = 'output_files/o_rxncon_%s' % now_stamp()
    for opt, arg in options:
        if opt in ('-h', '--help'):
            print_help()
            sys.exit(0)
        elif opt in ('-o', '--output-dir'):
            out_dir = arg

    # parse args
    nargs = len(argv)
    # required args
    if  nargs < 2:
        usage('Missing paths to reactions and contingencies CSV files.')
        sys.exit(1)
    path_rxn,path_con = argv[0:2]
    path_bool = argv[2] if nargs > 2 else None
    path_bterms = argv[3] if nargs > 3 else None
#===============================================================================
# import CSV data
#===============================================================================
    rxn = read_reactions_csv(path_rxn)
    print [str(r) for r in rxn]
    bol = read_booleans_csvs(path_bool,path_bterms, check=True)
    #print [str(b) for b in bol]
    con = read_contingencies_csv(path_con)
    #print [str(c) for c in con]
#===============================================================================
# construct the ESRGraph
#===============================================================================
    esr = ESRGraph()
    esr.add_reactions(rxn)
    esr.add_booleans(bol) # TODO: booleans can be automatically re-ordered to pass data consistency check
    esr.add_contingencies(con)
    rm = esr.remove_small_components()
#===============================================================================
# write_esr
#===============================================================================
    try:
        mkdir_p(out_dir, mode=0755)
        out_fn = out_fn_ext(out_dir,ESR_FN_PREFIX)
        write_esr(esr, out_fn, json=True)
    except:
        exc_info = sys.exc_info()
        print "Unexpected error:", exc_info[1]
        #import traceback; traceback.print_tb(exc_info[2])
        sys.exit(1)
#===============================================================================
# report
#===============================================================================
    print 'Good news, the RxnCon data has been imported and the corresponding eSR graph has been created and exported.'
    # TODO: send removed data to MK for mistakes detection
    print 'The following connected components have been pruned:'
    sys.stdout.write('\t')
    print '\n\t'.join(', '.join(str(c) for c in cc) for cc in rm)
    print 'The eSR graph ("%s" files) is in the "%s" folder.' % (out_fn('*'), out_dir)

    img_ext='pdf'
    print 'To plot you can run:'
    print '\tdot -x -Goverlap=scale -T%s %s > %s' % (img_ext, out_fn('dot'), out_fn(img_ext))

    sys.exit(0)
